#pragma once

#include "MyData.h"

namespace cv
{
	class Task_2
	{
		cv::MyData MyData;
		cv::Mat fGrImg1;
		cv::Mat fGrImg2;
		cv::Mat fRGBImg1;
		cv::Mat fRGBImg2;
		int32_t pxMinVal;
		int32_t pxMaxVal;

		enum oprt {add, mult, pw, dev, mSqrt, mLog};
	public:
		Task_2();
		void fLoad();
		void imProc(cv::Mat* tmpF, oprt oprt, int32_t value);
		void pxMinMaxValue(cv::Mat* tmpF, cv::Mat* tmpV);
		void imNorm(cv::Mat* tmpF);
		void imWriteAll(std::string fPath, std::string fPrefix);
		void imSum(cv::Mat* tmpF1, cv::Mat* tmpF2);
		void imMultIm(cv::Mat* tmpF1, cv::Mat* tmpF2);
		void imOverlay(cv::Mat* tmpF1, cv::Mat* tmpF2);
		void imDevByIm(cv::Mat* tmpF1, cv::Mat* tmpF2);
		void imLog(cv::Mat* tmpF);
	};
} // cv namespace end