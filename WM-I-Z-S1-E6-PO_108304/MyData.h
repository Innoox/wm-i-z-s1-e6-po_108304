#pragma once

#include <iostream>
#include <opencv2/opencv.hpp>

namespace cv
{
	class MyData
	{
	public:
		// Initital data to the first run
		// Variables declaration section
		std::string fNameSourcePath;
		std::string fNameMainResultPath;
		std::string fGreyName1;
		std::string fGreyName2;
		std::string fRGBName1;
		std::string fRGBName2;

		// Structure declaration section

		// BMP Strusture:

		/*
			BMP				-	is a binary file in the little-indian format.
			Little-indian	-	represent the order of bytes within a multi-byte number
								so that the least significant bytes come first and more
								significant last.
			File Header		-	all BMP images starts with a five element header

			A peculiarity of the BMP image format is that, if the height is negative,
			you have the origin of the image in the top left corner.
			If the height is a positive number, the origin of the image is at the bottom left corner.
			For simplicity, we will consider only the case when the image height is a positive number
			and the origin is always in the bottom left corner.
		*/

		/*
			Basic BMP information
			{
				"BMPFileHeader": [
					"Header"		- 14 bytes	- Windows structure: BITMAPFILEHEADER
					"Signature"		- 2 bytes	== 16 bit	- 'BM'	- 0x424D
					"FileSize"		- 4 bytes	== 32 bit	- uint32_t
					"Reserved"		- 4 bytes	==   -		- 2 * uint16_t
					"DataOffset"	- 4 bytes	==   -		- uint32_t
				],
				"BMPInfoHeader": [
					"Header"		- 40 bytes	- Windows structure: BITMAPINFOHEADER
					"Size"			- 4 bytes	== 32 bit	- uint32_t
					"Width"			- 4 bytes	==   -		- uint32_t
					"Height"		- 4 bytes	==   -		- uint32_t
					"Planes"		- 2 bytes	== 16 bit	- uint16_t	- 1
					"BitCount"		- 2 bytes	==   -		- uint16_t:
																		- 1		= monochrome. ColorNums = 2
																		- 2		= grayscale. ColorNums = 4
																		- 4		= 4 bit palletized. ColorNums = 16
																		- 8		= 8 bit palletized. ColorNums = 256
																		- 16	= 16 bit palletized. ColorNums = 65536
																		- 24	= 24 bit palletized. ColorNums = >16m
																		- 32	= 32 bit palletized. ColorNums = >4*10^9
					"Compression"	- 4 bytes	== 32 bit	- uint32_t	- 0, for no compression
					"ImageSize"		- 4 bytes	== 32 bit	- uint32_t	- 0 because no compression
					"MapX"			- 4 bytes	==   -		- uint32_t
					"MapY"			- 4 bytes	==	 -		- uint32_t
					"ColorUsed"		- 4 bytes	==	 -		- uint32_t	- No. color indexes in the color table.
																		  Use 0 for the max number of colors allowed by
																		  BitCount
					"ColorImprt"	- 4 bytes	==	 -		- uint32-t	- No. of colors used for displaying the bitmap.
																		  If 0 all colors are required
				],
				"BMPColorHeader": [
					"RedMask"		- 4 bytes	== uint32_t	- 0x00ff0000
					"GreenMask"		- 4 bytes	== uint32_t	- 0x0000ff00
					"BlueMask"		- 4 bytes	== uint32_t	- 0x000000ff
					"AlphaMask"		- 4 bytes	== uint32_t	- 00xff00000
					"ColorSpace"	- 4 bytes	== uint32_t	- 0x73524742
					"Reserved"		- 4 bytes	== uint32_t - 0x00
				]
			}
		*/;

	#pragma pack(push, 1)
		/*
			Without pragma pack(push, 1) the size of thus structures
			is 16 bytes, I need 14 bytes and it solves the problem
		*/
		// BMPFileHeader
		struct BMPFileHeader
		{
			uint16_t iFileType{ 0 };
			uint32_t iFileSize{ 0 };
			uint16_t iReserved1{ 0 };
			uint16_t iReserved2{ 0 };
			uint32_t iDataOffset{ 0 };
		};

		// BMPInfoHeader
		struct BMPInfoHeader
		{
			uint32_t iFileSize{ 0 };
			int32_t iFileWidth{ 0 };
			int32_t iFileHeight{ 0 };
			uint16_t iPlanes{ 0 };
			uint16_t iBitCount{ 0 };
			uint32_t iCompression{ 0 };
			uint32_t iSize{ 0 };
			int32_t iXPxPerMeter{ 0 };
			int32_t iYPxPerMeter{ 0 };
			uint32_t iColorUsed{ 0 };
			uint32_t iColorImportant{ 0 };
		};

		// BMPColorHeader
		struct BMPColorHeader
		{
			uint32_t iRedMask{ 0 };
			uint32_t iGreenMask{ 0 };
			uint32_t iBlueMask{ 0 };
			uint32_t iAlphaMask{ 0 };
			uint32_t iColorSpaceType{ 0 };
			uint32_t unused[16];
		};

		// Container for the pixel data
		struct Vec3b
		{
			uint8_t BlueMask{ 0 };
			uint8_t GreenMask{ 0 };
			uint8_t RedMask{ 0 };
		};
	#pragma pack(pop)

	public:
		MyData();
	};
} // cv namespace end