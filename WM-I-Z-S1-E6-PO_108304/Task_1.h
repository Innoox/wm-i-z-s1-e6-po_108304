#pragma once

#include "MyData.h"

namespace cv
{
	class Task_1
	{
		cv::MyData MyData;
		cv::Mat fGrImg1;
		cv::Mat fGrImg2;
		cv::Mat fRGBImg1;
		cv::Mat fRGBImg2;
		int32_t fMaxGRWidth;
		int32_t fMaxGRHeight;
		int32_t fMaxRGBWidth;
		int32_t fMaxRGBHeight;
	public:
		Task_1();
		void fLoad();
		void fGeomResize();
		void fRasterResize();
		void imProc(cv::Mat* fNameSrc, cv::Mat* fNameRes);
	};
} // cv namespace end