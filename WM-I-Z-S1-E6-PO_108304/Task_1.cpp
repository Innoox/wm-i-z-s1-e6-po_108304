#include "Task_1.h"

cv::Task_1::Task_1()
{
	this->fLoad();
	this->fGeomResize();
	this->fRasterResize();
}

// Loading test images and define it size.
void cv::Task_1::fLoad()
{
	this->fGrImg1 = cv::imread(this->MyData.fNameSourcePath + this->MyData.fGreyName1, cv::IMREAD_GRAYSCALE);
	this->fGrImg2 = cv::imread(this->MyData.fNameSourcePath + this->MyData.fGreyName2, cv::IMREAD_GRAYSCALE);
	this->fRGBImg1 = cv::imread(this->MyData.fNameSourcePath + this->MyData.fRGBName1, cv::IMREAD_COLOR);
	this->fRGBImg2 = cv::imread(this->MyData.fNameSourcePath + this->MyData.fRGBName2, cv::IMREAD_COLOR);
	this->fMaxGRWidth = (this->fGrImg1.cols > this->fGrImg2.cols) ? this->fGrImg1.cols : this->fGrImg2.cols;
	this->fMaxGRHeight = (this->fGrImg1.rows > this->fGrImg2.rows) ? this->fGrImg1.rows : this->fGrImg2.rows;
	this->fMaxRGBWidth = (this->fRGBImg1.cols > this->fRGBImg2.cols) ? this->fRGBImg1.cols : this->fRGBImg2.cols;
	this->fMaxRGBHeight = (this->fRGBImg1.rows > this->fRGBImg2.rows) ? this->fRGBImg1.rows : this->fRGBImg2.rows;
}

void cv::Task_1::fGeomResize()
{
	std::string GeomResizePath = this->MyData.fNameMainResultPath + "Task_1/GeomResize/";
	std::string GeomResizePrefix = "GeomResize_";
	cv::Mat GRResultImage1(this->fMaxGRWidth, this->fMaxGRHeight, this->fGrImg1.type(), cv::Scalar(0, 0, 0));
	cv::Mat GRResultImage2(this->fMaxGRWidth, this->fMaxGRHeight, this->fGrImg2.type(), cv::Scalar(0, 0, 0));
	cv::Mat RGBResultImage1(this->fMaxRGBWidth, this->fMaxRGBHeight, this->fRGBImg1.type(), cv::Scalar(0, 0, 0));
	cv::Mat RGBResultImage2(this->fMaxRGBWidth, this->fMaxRGBHeight, this->fRGBImg2.type(), cv::Scalar(0, 0, 0));

	this->imProc(&this->fGrImg1, &GRResultImage1);
	this->imProc(&this->fGrImg2, &GRResultImage2);
	this->imProc(&this->fRGBImg1, &RGBResultImage1);
	this->imProc(&this->fRGBImg2, &RGBResultImage2);

	cv::imwrite(GeomResizePath + GeomResizePrefix + this->MyData.fGreyName1, GRResultImage1);
	cv::imwrite(GeomResizePath + GeomResizePrefix + this->MyData.fGreyName2, GRResultImage2);
	cv::imwrite(GeomResizePath + GeomResizePrefix + this->MyData.fRGBName1, RGBResultImage1);
	cv::imwrite(GeomResizePath + GeomResizePrefix + this->MyData.fRGBName2, RGBResultImage2);

}

void cv::Task_1::fRasterResize()
{
	std::string RasterResizePath = this->MyData.fNameMainResultPath + "Task_1/RasterResize/";
	std::string RasterResizePrefix = "RasterResize_";
	cv::Mat GRResultImage1(this->fMaxGRWidth, this->fMaxGRHeight, this->fGrImg1.type(), cv::Scalar(0, 0, 0));
	cv::Mat GRResultImage2(this->fMaxGRWidth, this->fMaxGRHeight, this->fGrImg2.type(), cv::Scalar(0, 0, 0));
	cv::Mat RGBResultImage1(this->fMaxRGBWidth, this->fMaxRGBHeight, this->fRGBImg1.type(), cv::Scalar(0, 0, 0));
	cv::Mat RGBResultImage2(this->fMaxRGBWidth, this->fMaxRGBHeight, this->fRGBImg2.type(), cv::Scalar(0, 0, 0));

	cv::resize(this->fGrImg1, GRResultImage1, cv::Size(this->fMaxGRWidth, this->fMaxGRHeight), 0, 0, cv::INTER_LINEAR);
	cv::resize(this->fGrImg2, GRResultImage2, cv::Size(this->fMaxGRWidth, this->fMaxGRHeight), 0, 0, cv::INTER_LINEAR);
	cv::resize(this->fRGBImg1, RGBResultImage1, cv::Size(this->fMaxRGBWidth, this->fMaxRGBHeight), 0, 0, cv::INTER_LINEAR);
	cv::resize(this->fRGBImg2, RGBResultImage2, cv::Size(this->fMaxRGBWidth, this->fMaxRGBHeight), 0, 0, cv::INTER_LINEAR);

	cv::imwrite(RasterResizePath + RasterResizePrefix + this->MyData.fGreyName1, GRResultImage1);
	cv::imwrite(RasterResizePath + RasterResizePrefix + this->MyData.fGreyName2, GRResultImage2);
	cv::imwrite(RasterResizePath + RasterResizePrefix + this->MyData.fRGBName1, RGBResultImage1);
	cv::imwrite(RasterResizePath + RasterResizePrefix + this->MyData.fRGBName2, RGBResultImage2);

	/*
	
		cv::Mat imRes(tmpF.cols * scalVal, tmpF.rows * scalVal, tmpF.type(), cv::Scalar(0, 0, 0));
		for (size_t mapx = 0; mapx < tmpF.cols; ++mapx) {
			for (size_t mapy = 0; mapy < tmpF.rows; ++mapy) {
				if (tmpF.channels() == 1) {
					imRes.at<uchar>(mapx * scalVal, mapy * scalVal) = tmpF.at<uchar>(mapx, mapy);
				}
				else {
					for (size_t it = 0; it < tmpF.channels(); ++it) {
						imRes.at<cv::Vec3b>(mapx * scalVal, mapy * scalVal)[it] = tmpF.at<cv::Vec3b>(mapx, mapy)[it];
					}
				}
			}
		}

		std::string SclPath = "Task_4/Rotation/";
		std::string SclPrefix = "Inrepolation_";
		cv::imwrite(this->MyData.fNameMainResultPath + SclPath + fName, imRes);

		for (size_t mapx = 0; mapx < imRes.cols; ++mapx) {
			for (size_t mapy = 0; mapy < imRes.rows; ++mapy) {
				int redMask = 0;
				int greebMask = 0;
				int blueMask = 0;
				int mColor = 0;
				double cnt = 1;
				//cv::Mat imTmp = imRes.clone();
				if (tmpF.channels() == 1) {
					if (imRes.at<uchar>(mapx, mapy) == 0) {
						for (int mapxOf = -1; mapxOf < 2; ++mapxOf) {
							for (int mapyOf = -1; mapyOf < 2; ++mapyOf) {
								int mapxSafe = (((mapx + mapxOf) > (imRes.rows - 2)) || ((mapx + mapxOf) < 0)) ? mapx : mapx + mapxOf;
								int mapySafe = (((mapy + mapyOf) > (imRes.cols - 2)) || ((mapy + mapyOf) < 0)) ? mapy : mapy + mapyOf;
								if (imRes.at<uchar>(mapxSafe, mapySafe) > 0) {
									mColor += imRes.at<uchar>(mapxSafe, mapySafe);
									++cnt;
								}
							}
						}
						imRes.at<uchar>(mapx, mapy) = mColor / cnt;
					}
				}
				else {
					if ((imRes.at<cv::Vec3b>(mapx, mapy)[0] == 0) &&
						(imRes.at<cv::Vec3b>(mapx, mapy)[1] == 0) &&
						(imRes.at<cv::Vec3b>(mapx, mapy)[2] == 0)) {
						for (int mapxOf = -1; mapxOf < 2; ++mapxOf) {
							for (int mapyOf = -1; mapyOf < 2; ++mapyOf) {
								int mapxSafe = (((mapx + mapxOf) > (imRes.rows - 2)) || ((mapx + mapxOf) < 0)) ? mapx : mapx + mapxOf;
								int mapySafe = (((mapy + mapyOf) > (imRes.cols - 2)) || ((mapy + mapyOf) < 0)) ? mapy : mapy + mapyOf;
								if ((imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[0] > 0) ||
									(imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[1] > 0) ||
									(imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[2] > 0)) {
									redMask += imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[0];
									greebMask += imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[1];
									blueMask += imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[2];
									++cnt;
								}
							}
						}
						imRes.at<cv::Vec3b>(mapx, mapy)[0] = redMask / cnt;
						imRes.at<cv::Vec3b>(mapx, mapy)[1] = greebMask / cnt;
						imRes.at<cv::Vec3b>(mapx, mapy)[2] = blueMask / cnt;
					}
				}
			}
		}
		cv::imwrite(this->MyData.fNameMainResultPath + SclPath + SclPrefix + fName, imRes);
	
	*/

}

void cv::Task_1::imProc(cv::Mat* fNameSrc, cv::Mat* fNameRes)
{
	int32_t startPx = (fNameRes->cols - fNameSrc->cols) / 2;
	int32_t startPy = (fNameRes->rows - fNameSrc->rows) / 2;
	// Test command
	// std::cout << "Start points : " << startPx << " x " << startPy << std::endl;
	for (size_t mapx = 0; mapx < fNameSrc->cols; ++mapx) {
		for (size_t mapy = 0; mapy < fNameSrc->rows; ++mapy) {
			if (fNameSrc->channels() == 1) {
				fNameRes->at<uchar>(mapx + startPx, mapy + startPy) = fNameSrc->at<uchar>(mapx, mapy);
			}
			else {
				fNameRes->at<cv::Vec3b>(mapx + startPx, mapy + startPy) = fNameSrc->at<cv::Vec3b>(mapx, mapy);
				//for (size_t it = 0; it < fNameSrc->channels(); ++it) {
				//	fNameRes->at<cv::Vec3b>(mapx, mapy)[it] = fNameSrc->at<cv::Vec3b>(mapx, mapy)[it];
				//}
			}
		}
	}
}

/*

	cv::Mat imRes(tmpF.cols * scalVal, tmpF.rows * scalVal, tmpF.type(), cv::Scalar(0, 0, 0));
	for (size_t mapx = 0; mapx < tmpF.cols; ++mapx) {
		for (size_t mapy = 0; mapy < tmpF.rows; ++mapy) {
			if (tmpF.channels() == 1) {
				imRes.at<uchar>(mapx * scalVal, mapy * scalVal) = tmpF.at<uchar>(mapx, mapy);
			}
			else {
				for (size_t it = 0; it < tmpF.channels(); ++it) {
					imRes.at<cv::Vec3b>(mapx * scalVal, mapy * scalVal)[it] = tmpF.at<cv::Vec3b>(mapx, mapy)[it];
				}
			}
		}
	}

	std::string SclPath = "Task_4/Rotation/";
	std::string SclPrefix = "Inrepolation_";
	cv::imwrite(this->MyData.fNameMainResultPath + SclPath + fName, imRes);

	for (size_t mapx = 0; mapx < imRes.cols; ++mapx) {
		for (size_t mapy = 0; mapy < imRes.rows; ++mapy) {
			int redMask = 0;
			int greebMask = 0;
			int blueMask = 0;
			int mColor = 0;
			double cnt = 1;
			//cv::Mat imTmp = imRes.clone();
			if (tmpF.channels() == 1) {
				if (imRes.at<uchar>(mapx, mapy) == 0) {
					for (int mapxOf = -1; mapxOf < 2; ++mapxOf) {
						for (int mapyOf = -1; mapyOf < 2; ++mapyOf) {
							int mapxSafe = (((mapx + mapxOf) > (imRes.rows - 2)) || ((mapx + mapxOf) < 0)) ? mapx : mapx + mapxOf;
							int mapySafe = (((mapy + mapyOf) > (imRes.cols - 2)) || ((mapy + mapyOf) < 0)) ? mapy : mapy + mapyOf;
							if (imRes.at<uchar>(mapxSafe, mapySafe) > 0) {
								mColor += imRes.at<uchar>(mapxSafe, mapySafe);
								++cnt;
							}
						}
					}
					imRes.at<uchar>(mapx, mapy) = mColor / cnt;
				}
			}
			else {
				if ((imRes.at<cv::Vec3b>(mapx, mapy)[0] == 0) &&
					(imRes.at<cv::Vec3b>(mapx, mapy)[1] == 0) &&
					(imRes.at<cv::Vec3b>(mapx, mapy)[2] == 0)) {
					for (int mapxOf = -1; mapxOf < 2; ++mapxOf) {
						for (int mapyOf = -1; mapyOf < 2; ++mapyOf) {
							int mapxSafe = (((mapx + mapxOf) > (imRes.rows - 2)) || ((mapx + mapxOf) < 0)) ? mapx : mapx + mapxOf;
							int mapySafe = (((mapy + mapyOf) > (imRes.cols - 2)) || ((mapy + mapyOf) < 0)) ? mapy : mapy + mapyOf;
							if ((imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[0] > 0) ||
								(imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[1] > 0) ||
								(imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[2] > 0)) {
								redMask += imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[0];
								greebMask += imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[1];
								blueMask += imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[2];
								++cnt;
							}
						}
					}
					imRes.at<cv::Vec3b>(mapx, mapy)[0] = redMask / cnt;
					imRes.at<cv::Vec3b>(mapx, mapy)[1] = greebMask / cnt;
					imRes.at<cv::Vec3b>(mapx, mapy)[2] = blueMask / cnt;
				}
			}
		}
	}
	cv::imwrite(this->MyData.fNameMainResultPath + SclPath + SclPrefix + fName, imRes);

*/