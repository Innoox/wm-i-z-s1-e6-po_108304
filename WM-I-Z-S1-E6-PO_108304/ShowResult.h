#pragma once

#include "MyData.h"

namespace cv
{
	class ShowResult
	{
		std::string SourcePath;
		std::string ResultPath;

		std::string fGreyName1;
		std::string fGreyName2;
		std::string fRGBName1;
		std::string fRGBName2;

		cv::Mat fGrImg1;
		cv::Mat fGrImg2;
		cv::Mat fRGBImg1;
		cv::Mat fRGBImg2;

		std::string CurrentTaskPath;
		std::string fName1;
		std::string fName2;
		std::string fName3;
		std::string fName4;

		std::string fNameN1;
		std::string fNameN2;
		std::string fNameN3;
		std::string fNameN4;

	public:

		ShowResult();

		void fLoad();

		void ShowResultT_1_1();
		void ShowResultT_1_2();
		void ShowResultT_1_3();
		void ShowResultT_1_4();

		void ShowResultT_2_1();
		void ShowResultT_2_2();
		void ShowResultT_2_3();
		void ShowResultT_2_4();
		void ShowResultT_2_5();
		void ShowResultT_2_6();
		void ShowResultT_2_7();
		void ShowResultT_2_8();
		void ShowResultT_2_9();
		void ShowResultT_2_10();


		void ShowResultT_3_1();
		void ShowResultT_3_2();
		void ShowResultT_3_3();
		void ShowResultT_3_4();
		void ShowResultT_3_5();
		void ShowResultT_3_6();
		void ShowResultT_3_7();
		void ShowResultT_3_8();
		void ShowResultT_3_9();
		void ShowResultT_3_10();


		void ShowResultT_4_1();
		void ShowResultT_4_2();
		void ShowResultT_4_3();
		void ShowResultT_4_4();
		void ShowResultT_4_5();
		void ShowResultT_4_6();
		void ShowResultT_4_7();
		void ShowResultT_4_8();

	};
}