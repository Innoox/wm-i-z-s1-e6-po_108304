#include <iostream>
#include "Task_1.h"
#include "Task_2.h"
#include "Task_4.h"
#include "ShowResult.h"

int main(int argc, char** argv)
{
	cv::Task_1 myTask_1;
	cv::Task_2 myTask_2;
	cv::Task_4 myTask_4;
	cv::ShowResult ShowRes;
	int mainTaskNumber = 99;
	int subTaskNumber = 0;

	while (mainTaskNumber != 0)
	{
		// Intro section
		std::cout << "\nPrzetwarzanie obrazow.\nSprawozdanie z laboratorium.\n";
		std::cout << "\nAuthor:\t\tKostiantyn Ostapenko\nNr. albumu:\t108304\n";
		std::cout << "\nPlease select task:\n"
			<< "1. Operacje ujednoliczania obrazow.\n"
			<< "2. Operacje arytmetyczne na obrazach szarych.\n"
			<< "3. Operacje arytmetyczne na obrazach barwowych\n"
			<< "4. Operacje geometryczne na obrazie.\n"
			<< "0. Exit\n";

		std::cin >> mainTaskNumber;

		switch (mainTaskNumber)
		{
		case 1:
			std::cout << "\nPlease select subtask:\n"
				<< "1. ujednolicenie obrazow szarych geometryczne (liczba wierszy i kolumn w obrazie).\n"
				<< "2. ujednolicenie obrazow szarych rozdzielczosciowe (w rastrze ujednolicenie rastra).\n"
				<< "3. ujednolicenie obrazow RGB geometryczne (liczba wierszy i kolumn w obrazie).\n"
				<< "4. ujednolicenie obrazow RGB rozdzielczosciowe (w rozmiarze rastra).\n"
				<< "0. Exit\n"
				<< "\"ESC\" - close the images.\n";
			std::cin >> subTaskNumber;
			switch (subTaskNumber)
			{
			case 1:
				ShowRes.ShowResultT_1_1();
				break;
			case 2:
				ShowRes.ShowResultT_1_2();
				break;
			case 3:
				ShowRes.ShowResultT_1_3();
				break;
			case 4:
				ShowRes.ShowResultT_1_4();
				break;
			case 0:
				return 0;
			default:
				break;
			}
		case 2:
			std::cout << "\nPlease select subtask:\n"
				<< "1. sumowanie (okreslonej) stalej z obrazem.\n"
				<< "2. mnozenie obrazu przez zadana liczbe.\n"
				<< "3. mieszanie obrazow z okreslonym wspolczynnikiem.\n"
				<< "4. potegowanie obrazu (z zadana potega).\n"
				<< "5. dzielenie obrazu przez (zadana) liczbe.\n"
				<< "6. pierwiastkowanie obrazu.\n"
				<< "7. logarytmowanie obrazu.\n"
				<< "8. sumowanie dwoch obrazow.\n"
				<< "9. mnozenie obrazu przez inny obraz.\n"
				<< "10. dzielenie obrazu przez inny obraz.\n"
				<< "0. Exit\n"
				<< "\"ESC\" - close the images.\n";
			std::cin >> subTaskNumber;
			switch (subTaskNumber)
			{
			case 1:
				ShowRes.ShowResultT_2_1();
				break;
			case 2:
				ShowRes.ShowResultT_2_2();
				break;
			case 3:
				ShowRes.ShowResultT_2_3();
				break;
			case 4:
				ShowRes.ShowResultT_2_4();
				break;
			case 5:
				ShowRes.ShowResultT_2_5();
				break;
			case 6:
				ShowRes.ShowResultT_2_6();
				break;
			case 7:
				ShowRes.ShowResultT_2_7();
				break;
			case 8:
				ShowRes.ShowResultT_2_8();
				break;
			case 9:
				ShowRes.ShowResultT_2_9();
				break;
			case 10:
				ShowRes.ShowResultT_2_10();
				break;
			case 0:
				return 0;
			default:
				break;
			}
			break;
		case 3:
			std::cout << "\nPlease select subtask:\n"
				<< "1. sumowanie (okreslonej) stalej z obrazem.\n"
				<< "2. mnozenie obrazu przez zadana liczbe.\n"
				<< "3. mieszanie obrazow z okreslonym wspolczynnikiem.\n"
				<< "4. potegowanie obrazu (z zadana potega).\n"
				<< "5. dzielenie obrazu przez (zadana) liczbe.\n"
				<< "6. pierwiastkowanie obrazu.\n"
				<< "7. logarytmowanie obrazu.\n"
				<< "8. sumowanie dwoch obrazow.\n"
				<< "9. mnozenie obrazu przez inny obraz.\n"
				<< "10. dzielenie obrazu przez inny obraz.\n"
				<< "0. Exit\n"
				<< "\"ESC\" - close the images.\n";
			std::cin >> subTaskNumber;
			switch (subTaskNumber)
			{
			case 1:
				ShowRes.ShowResultT_3_1();
				break;
			case 2:
				ShowRes.ShowResultT_3_2();
				break;				
			case 3:					
				ShowRes.ShowResultT_3_3();
				break;				
			case 4:					
				ShowRes.ShowResultT_3_4();
				break;				
			case 5:					
				ShowRes.ShowResultT_3_5();
				break;				
			case 6:					
				ShowRes.ShowResultT_3_6();
				break;				
			case 7:					
				ShowRes.ShowResultT_3_7();
				break;
			case 8:
				ShowRes.ShowResultT_3_8();
				break;
			case 9:
				ShowRes.ShowResultT_3_9();
				break;
			case 10:
				ShowRes.ShowResultT_3_10();
				break;
			case 0:
				return 0;
			default:
				break;
			}
			break;
		case 4:
			std::cout << "\nPlease select subtask:\n"
				<< "1. przemieszczenie obrazu o zadany wektor.\n"
				<< "2. jednorodne skalowanie obrazu.\n"
				<< "3. niejednorodne skalowanie obrazu.\n"
				<< "4. obracanie obrazu o dowolny kat.\n"
				<< "5. symetrie wzgledem osi ukladu.\n"
				<< "6. symetrie wzgledem zadanej prostej.\n"
				<< "7. wycinanie fragmentow obrazu.\n"
				<< "8. kopiowanie fragmentow obrazow.\n"
				<< "0. Exit\n"
				<< "\"ESC\" - close the images.\n";
			std::cin >> subTaskNumber;
			switch (subTaskNumber)
			{
			case 1:
				ShowRes.ShowResultT_4_1();
				break;
			case 2:
				ShowRes.ShowResultT_4_2();
				break;
			case 3:
				ShowRes.ShowResultT_4_3();
				break;
			case 4:
				ShowRes.ShowResultT_4_4();
				break;
			case 5:
				ShowRes.ShowResultT_4_5();
				break;
			case 6:
				ShowRes.ShowResultT_4_6();
				break;
			case 7:
				ShowRes.ShowResultT_4_7();
				break;
			case 8:
				ShowRes.ShowResultT_4_8();
				break;
			case 0:
				return 0;
			default:
				break;
			}
			break;
		case 0:
			return 0;
			break;
		default:
			break;
		}
	}

	return 0;
}