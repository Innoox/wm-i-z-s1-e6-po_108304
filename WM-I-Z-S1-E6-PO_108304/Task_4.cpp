#define _USE_MATH_DEFINES

#include "Task_4.h"
#include <cmath>

cv::Task_4::Task_4()
{

	this->fLoad();
	this->rmFragment(this->fGrImg1, this->MyData.fGreyName1, 100, 100, 50, 15);
	this->rmFragment(this->fGrImg2, this->MyData.fGreyName2, 75, 200, 25, 5);
	this->rmFragment(this->fRGBImg1, this->MyData.fRGBName1, 210, 50, 10, 10);
	this->rmFragment(this->fRGBImg2, this->MyData.fRGBName2, 150, 100, 100, 100);

	this->fLoad();
	this->copyFragment(this->fGrImg1, this->MyData.fGreyName1, 110, 210, 45, 35);
	this->copyFragment(this->fGrImg2, this->MyData.fGreyName2, 210, 450, 15, 55);
	this->copyFragment(this->fRGBImg1, this->MyData.fRGBName1, 120, 210, 35, 25);
	this->copyFragment(this->fRGBImg2, this->MyData.fRGBName2, 330, 450, 25, 15);

	this->fLoad();
	this->moveByVector(this->fGrImg1, this->MyData.fGreyName1, 100, 50);
	this->moveByVector(this->fGrImg2, this->MyData.fGreyName2, 50, -75);
	this->moveByVector(this->fRGBImg1, this->MyData.fRGBName1, -25, -125);
	this->moveByVector(this->fRGBImg2, this->MyData.fRGBName2, -35, 65);

	this->fLoad();
	this->imSymmetry(this->fGrImg1, "ByX_" + this->MyData.fGreyName1, cv::Task_4::MySymmetry::x_axis);
	this->imSymmetry(this->fGrImg1, "ByY_" + this->MyData.fGreyName1, cv::Task_4::MySymmetry::y_axis);

	this->imSymmetry(this->fGrImg2, "ByX_" + this->MyData.fGreyName2, MySymmetry::x_axis);
	this->imSymmetry(this->fGrImg2, "ByY_" + this->MyData.fGreyName2, MySymmetry::y_axis);

	this->imSymmetry(this->fRGBImg1, "ByX_" + this->MyData.fRGBName1, MySymmetry::x_axis);
	this->imSymmetry(this->fRGBImg1, "ByY_" + this->MyData.fRGBName1, MySymmetry::y_axis);

	this->imSymmetry(this->fRGBImg2, "ByX_" + this->MyData.fRGBName2, MySymmetry::x_axis);
	this->imSymmetry(this->fRGBImg2, "ByY_" + this->MyData.fRGBName2, MySymmetry::y_axis);

	this->imSymmetry(this->fGrImg1, "ByMX_" + this->MyData.fGreyName1, MySymmetry::mx_axis);
	this->imSymmetry(this->fGrImg1, "ByMY_" + this->MyData.fGreyName1, MySymmetry::my_axis);

	this->imSymmetry(this->fGrImg2, "ByMX_" + this->MyData.fGreyName2, MySymmetry::mx_axis);
	this->imSymmetry(this->fGrImg2, "ByMY_" + this->MyData.fGreyName2, MySymmetry::my_axis);

	this->imSymmetry(this->fRGBImg1, "ByMX_" + this->MyData.fRGBName1, MySymmetry::mx_axis);
	this->imSymmetry(this->fRGBImg1, "ByMY_" + this->MyData.fRGBName1, MySymmetry::my_axis);

	this->imSymmetry(this->fRGBImg2, "ByMX_" + this->MyData.fRGBName2, MySymmetry::mx_axis);
	this->imSymmetry(this->fRGBImg2, "ByMY_" + this->MyData.fRGBName2, MySymmetry::my_axis);

	this->imRotate(this->fGrImg1, this->MyData.fGreyName1, 45);
	this->imRotate(this->fGrImg2, this->MyData.fGreyName2, 50);
	this->imRotate(this->fRGBImg1, this->MyData.fRGBName1, 55);
	this->imRotate(this->fRGBImg2, this->MyData.fRGBName2, 25);

	this->homoScal(this->fGrImg1, this->MyData.fGreyName1, 2, 0);
	this->homoScal(this->fGrImg2, this->MyData.fGreyName2, 2, 0);
	this->homoScal(this->fRGBImg1, this->MyData.fRGBName1, 2, 0);
	this->homoScal(this->fRGBImg2, this->MyData.fRGBName2, 2, 0);

	this->homoScal(this->fGrImg1, this->MyData.fGreyName1, 3, 1);
	this->homoScal(this->fGrImg2, this->MyData.fGreyName2, 3, 1);
	this->homoScal(this->fRGBImg1, this->MyData.fRGBName1, 3, 1);
	this->homoScal(this->fRGBImg2, this->MyData.fRGBName2, 3, 1);
}

// Loading test images and define it size.
void cv::Task_4::fLoad()
{
	this->fGrImg1 = cv::imread(this->MyData.fNameSourcePath + this->MyData.fGreyName1, cv::IMREAD_GRAYSCALE);
	this->fGrImg2 = cv::imread(this->MyData.fNameSourcePath + this->MyData.fGreyName2, cv::IMREAD_GRAYSCALE);
	this->fRGBImg1 = cv::imread(this->MyData.fNameSourcePath + this->MyData.fRGBName1, cv::IMREAD_COLOR);
	this->fRGBImg2 = cv::imread(this->MyData.fNameSourcePath + this->MyData.fRGBName2, cv::IMREAD_COLOR);
}

void cv::Task_4::rmFragment(cv::Mat tmpF, std::string fName, int width, int height, int posx, int posy)
{
	if (posx + width < tmpF.cols && posy + height < tmpF.rows && width != 0 && height != 0) {
		for (int mapx = 0; mapx < tmpF.cols; ++mapx) {
			for (int mapy = 0; mapy < tmpF.rows; ++mapy) {
				if (mapx >= posy && mapx <= (posy + width) && mapy >= posx && mapy <= (posx + height)) {
					if (tmpF.channels() == 1) {
						tmpF.at<uchar>(mapx, mapy) = 0;
					}
					else {
						for (size_t it = 0; it < tmpF.channels(); ++it) {
							tmpF.at<cv::Vec3b>(mapx, mapy)[it] = 0;
						}
					}
				}
			}
		}
	}
	std::string rmPath = "Task_4/RemoveFragment/";
	cv::imwrite(this->MyData.fNameMainResultPath + rmPath + fName, tmpF);
}

void cv::Task_4::copyFragment(cv::Mat tmpF, std::string fName, int width, int height, int posx, int posy)
{
	cv::Mat imRes(width + 1, height + 1, tmpF.type(), cv::Scalar(0, 0, 0));
	if (posx + width < tmpF.cols && posy + height < tmpF.rows && width != 0 && height != 0) {
		for (int mapx = 0; mapx < tmpF.cols; ++mapx) {
			for (int mapy = 0; mapy < tmpF.rows; ++mapy) {
				if (!(mapx >= posy && mapx <= (posy + width) && mapy >= posx && mapy <= (posx + height))) {
					if (tmpF.channels() == 1) {
						tmpF.at<uchar>(mapx, mapy) = 0;
					}
					else {
						for (size_t it = 0; it < tmpF.channels(); ++it) {
							tmpF.at<cv::Vec3b>(mapx, mapy)[it] = 0;
						}
					}
				}
				if (mapx >= posy && mapx <= (posy + width) && mapy >= posx && mapy <= (posx + height)) {
					if (tmpF.channels() == 1) {
						imRes.at<uchar>(mapx - posy, mapy - posx) = tmpF.at<uchar>(mapx, mapy);
					}
					else {
						for (size_t it = 0; it < tmpF.channels(); ++it) {
							imRes.at<cv::Vec3b>(mapx - posy, mapy - posx)[it] = tmpF.at<cv::Vec3b>(mapx, mapy)[it];
						}
					}
				}
			}
		}
	}
	std::string cpPath = "Task_4/CopyFragment/";
	std::string cpPrefix = "Cropped_";
	cv::imwrite(this->MyData.fNameMainResultPath + cpPath + fName, tmpF);
	cv::imwrite(this->MyData.fNameMainResultPath + cpPath + cpPrefix + fName, imRes);
	//cv::imshow(fName, tmpF);
	//cv::imshow(fName + "cropp", imRes);
	//cv::waitKey();
}

void cv::Task_4::moveByVector(cv::Mat tmpF, std::string fName, int posx, int posy)
{
	posx = 0 - posx;
	cv::Mat imRes(tmpF.cols, tmpF.rows, tmpF.type(), cv::Scalar(0, 0, 0));
	for (size_t mapx = 0; mapx < tmpF.cols; ++mapx) {
		for (size_t mapy = 0; mapy < tmpF.rows; ++mapy) {
			if (mapx + posx < imRes.cols && mapy + posy < imRes.rows) {
				if (tmpF.channels() == 1) {
					imRes.at<uchar>(mapx + posx, mapy + posy) = tmpF.at<uchar>(mapx, mapy);
				}
				else {
					for (size_t it = 0; it < tmpF.channels(); ++it) {
						imRes.at<cv::Vec3b>(mapx + posx, mapy + posy)[it] = tmpF.at<cv::Vec3b>(mapx, mapy)[it];
					}
				}
			}
		}
	}
	std::string mvPath = "Task_4/MoveFragment/";
	std::string mvPrefix = "Moved_";
	cv::imwrite(this->MyData.fNameMainResultPath + mvPath + mvPrefix + fName, imRes);
	//cv::imshow(fName, imRes);
	//cv::waitKey();
}

void cv::Task_4::imSymmetry(cv::Mat tmpF, std::string fName, MySymmetry axis)
{
	cv::Mat imRes(tmpF.cols, tmpF.rows, tmpF.type(), cv::Scalar(0, 0, 0));
	for (size_t mapx = 0; mapx < tmpF.cols; ++mapx) {
		for (size_t mapy = 0; mapy < tmpF.rows; ++mapy) {
			switch (axis)
			{
			case cv::Task_4::MySymmetry::x_axis:
				if (tmpF.channels() == 1) {
					imRes.at<uchar>(tmpF.cols - mapx - 1, mapy) = tmpF.at<uchar>(mapx, mapy);
				}
				else {
					for (size_t it = 0; it < tmpF.channels(); ++it) {
						imRes.at<cv::Vec3b>(tmpF.cols - mapx - 1, mapy)[it] = tmpF.at<cv::Vec3b>(mapx, mapy)[it];
					}
				}
				break;
			case cv::Task_4::MySymmetry::y_axis:
				if (tmpF.channels() == 1) {
					imRes.at<uchar>(mapx, tmpF.rows - mapy - 1) = tmpF.at<uchar>(mapx, mapy);
				}
				else {
					for (size_t it = 0; it < tmpF.channels(); ++it) {
						imRes.at<cv::Vec3b>(mapx, tmpF.rows -  mapy - 1)[it] = tmpF.at<cv::Vec3b>(mapx, mapy)[it];
					}
				}
				break;
			case cv::Task_4::MySymmetry::mx_axis:
				if (tmpF.channels() == 1) {
					if (mapx <= tmpF.cols / 2) {
						imRes.at<uchar>(mapx, mapy) = tmpF.at<uchar>(mapx, mapy);
					}
					else {
						imRes.at<uchar>(mapx, mapy) = tmpF.at<uchar>(tmpF.cols - mapx - 1, mapy);
					}
				}
				else {
					for (size_t it = 0; it < tmpF.channels(); ++it) {
						if (mapx <= tmpF.cols / 2) {
							imRes.at<cv::Vec3b>(mapx, mapy)[it] = tmpF.at<cv::Vec3b>(mapx, mapy)[it];
						}
						else {
							imRes.at<cv::Vec3b>(mapx, mapy)[it] = tmpF.at<cv::Vec3b>(tmpF.cols - mapx - 1, mapy)[it];
						}
					}
				}
				break;
			case cv::Task_4::MySymmetry::my_axis:
				if (tmpF.channels() == 1) {
					if (mapy <= tmpF.rows / 2) {
						imRes.at<uchar>(mapx, mapy) = tmpF.at<uchar>(mapx, mapy);
					}
					else {
						imRes.at<uchar>(mapx, mapy) = tmpF.at<uchar>(mapx, tmpF.rows - mapy - 1);
					}
				}
				else {
					for (size_t it = 0; it < tmpF.channels(); ++it) {
						if (mapy <= tmpF.rows / 2) {
							imRes.at<cv::Vec3b>(mapx, mapy)[it] = tmpF.at<cv::Vec3b>(mapx, mapy)[it];
						}
						else {
							imRes.at<cv::Vec3b>(mapx, mapy)[it] = tmpF.at<cv::Vec3b>(mapx, tmpF.rows - mapy - 1)[it];
						}
					}
				}
				break;
			default:
				break;
			}
		}
	}
	std::string SymPath;
	switch (axis)
	{
	case cv::Task_4::MySymmetry::x_axis:
	case cv::Task_4::MySymmetry::y_axis:
		SymPath = "Task_4/Symmetry/";
		break;
	case cv::Task_4::MySymmetry::mx_axis:
	case cv::Task_4::MySymmetry::my_axis:
		SymPath = "Task_4/SymmetryMiddle/";
		break;
	default:
		break;
	}
	cv::imwrite(this->MyData.fNameMainResultPath + SymPath + fName, imRes);
	//cv::imshow(fName, imRes);
	//cv::waitKey();
}

void cv::Task_4::imRotate(cv::Mat tmpF, std::string fName, int myAngle)
{
	cv::Mat imRes(tmpF.cols, tmpF.rows, tmpF.type(), cv::Scalar(0, 0, 0));
	//cv::Point2f newAxis((tmpF.cols - 1) / 2., (tmpF.rows - 1) / 2.);
	//cv::Mat imRot = cv::getRotationMatrix2D(newAxis, -myAngle, 1.);
	//cv::warpAffine(tmpF, imRes, imRot, tmpF.size());
	int newX0 = 0;
	int newY0 = 0;
	double newAngle = -(myAngle * M_PI) / 180;
	for (size_t mapx = 0; mapx < tmpF.cols; ++mapx) {
		for (size_t mapy = 0; mapy < tmpF.rows; ++mapy) {
			newX0 = ceil((mapx - tmpF.cols / 2.) * std::cos(newAngle) - (mapy - tmpF.rows / 2.) * std::sin(newAngle) + tmpF.cols / 2.);
			newY0 = ceil((mapx - tmpF.cols / 2.) * std::sin(newAngle) + (mapy - tmpF.rows / 2.) * std::cos(newAngle) + tmpF.rows / 2.);
			if (newY0 < tmpF.rows && newY0 >= 0 && newX0 >= 0 && newX0 < tmpF.cols) {
				if (tmpF.channels() == 1) {
					imRes.at<uchar>(newX0, newY0) = tmpF.at<uchar>(mapx, mapy);
				}
				else {
					for (size_t it = 0; it < tmpF.channels(); ++it) {
						imRes.at<cv::Vec3b>(newX0, newY0)[it] = tmpF.at<cv::Vec3b>(mapx, mapy)[it];
					}
				}
			}
		}
	}

	std::string RtPath = "Task_4/Rotation/";
	cv::imwrite(this->MyData.fNameMainResultPath + RtPath + fName, imRes);

	// interpolation
	for (size_t mapx = 0; mapx < tmpF.cols; ++mapx) {
		for (size_t mapy = 0; mapy < tmpF.rows; ++mapy) {
			int redMask = 0;
			int greebMask = 0;
			int blueMask = 0;
			int mColor = 0;
			double cnt = 1;
			//cv::Mat imTmp = imRes.clone();
			if (tmpF.channels() == 1) {
				if (imRes.at<uchar>(mapx, mapy) == 0) {
					for (int mapxOf = -1; mapxOf < 2; ++mapxOf) {
						for (int mapyOf = -1; mapyOf < 2; ++mapyOf) {
							int mapxSafe = (((mapx + mapxOf) > (tmpF.rows - 2)) || ((mapx + mapxOf) < 0)) ? mapx : mapx + mapxOf;
							int mapySafe = (((mapy + mapyOf) > (tmpF.cols - 2)) || ((mapy + mapyOf) < 0)) ? mapy : mapy + mapyOf;
							if (imRes.at<uchar>(mapxSafe, mapySafe) > 0) {
								mColor += imRes.at<uchar>(mapxSafe, mapySafe);
								++cnt;
							}
						}
					}
					imRes.at<uchar>(mapx, mapy) = mColor / cnt;
				}
			}
			else {
				if ((imRes.at<cv::Vec3b>(mapx, mapy)[0] == 0) &&
					(imRes.at<cv::Vec3b>(mapx, mapy)[1] == 0) &&
					(imRes.at<cv::Vec3b>(mapx, mapy)[2] == 0)) {
					for (int mapxOf = -1; mapxOf < 2; ++mapxOf) {
						for (int mapyOf = -1; mapyOf < 2; ++mapyOf) {
							int mapxSafe = (((mapx + mapxOf) > (tmpF.rows - 2)) || ((mapx + mapxOf) < 0)) ? mapx : mapx + mapxOf;
							int mapySafe = (((mapy + mapyOf) > (tmpF.cols - 2)) || ((mapy + mapyOf) < 0)) ? mapy : mapy + mapyOf;
							if ((imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[0] > 0) ||
								(imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[1] > 0) ||
								(imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[2] > 0)) {
								redMask += imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[0];
								greebMask += imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[1];
								blueMask += imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[2];
								++cnt;
							}
						}
					}
					imRes.at<cv::Vec3b>(mapx, mapy)[0] = redMask / cnt;
					imRes.at<cv::Vec3b>(mapx, mapy)[1] = greebMask / cnt;
					imRes.at<cv::Vec3b>(mapx, mapy)[2] = blueMask / cnt;
				}
			}
		}
	}
	std::string RtPrefix = "Interpolation_";
	cv::imwrite(this->MyData.fNameMainResultPath + RtPath + RtPrefix + fName, imRes);
}

void cv::Task_4::homoScal(cv::Mat tmpF, std::string fName, int scalValx, int scalValy)
{
	std::string SclPath = "Task_4/ScaleUnEqual/";
	if (scalValx > 1 && scalValy == 0) {
		scalValy = scalValx;
		SclPath = "Task_4/Scale/";
	}
	cv::Mat imRes(tmpF.cols, tmpF.rows, tmpF.type(), cv::Scalar(0, 0, 0));
	for (size_t mapx = 0; mapx < tmpF.cols; ++mapx) {
		for (size_t mapy = 0; mapy < tmpF.rows; ++mapy) {
			if ((scalValx * mapx) < tmpF.cols && scalValy * mapy < tmpF.rows) {
				if (tmpF.channels() == 1) {
					imRes.at<uchar>(mapx * scalValx, mapy * scalValy) = tmpF.at<uchar>(mapx, mapy);
				}
				else {
					for (size_t it = 0; it < tmpF.channels(); ++it) {
						imRes.at<cv::Vec3b>(mapx * scalValx, mapy * scalValy)[it] = tmpF.at<cv::Vec3b>(mapx, mapy)[it];
					}
				}
			}

		}
	}

	std::string SclPrefix = "Inrepolation_";
	cv::imwrite(this->MyData.fNameMainResultPath + SclPath + fName, imRes);

	for (size_t mapx = 0; mapx < imRes.cols; ++mapx) {
		for (size_t mapy = 0; mapy < imRes.rows; ++mapy) {
			int redMask = 0;
			int greebMask = 0;
			int blueMask = 0;
			int mColor = 0;
			double cnt = 1;
			//cv::Mat imTmp = imRes.clone();
			if (tmpF.channels() == 1) {
				if (imRes.at<uchar>(mapx, mapy) == 0) {
					for (int mapxOf = -1; mapxOf < 2; ++mapxOf) {
						for (int mapyOf = -1; mapyOf < 2; ++mapyOf) {
							int mapxSafe = (((mapx + mapxOf) > (imRes.rows - 2)) || ((mapx + mapxOf) < 0)) ? mapx : mapx + mapxOf;
							int mapySafe = (((mapy + mapyOf) > (imRes.cols - 2)) || ((mapy + mapyOf) < 0)) ? mapy : mapy + mapyOf;
							if (imRes.at<uchar>(mapxSafe, mapySafe) > 0) {
								mColor += imRes.at<uchar>(mapxSafe, mapySafe);
								++cnt;
							}
						}
					}
					imRes.at<uchar>(mapx, mapy) = mColor / cnt;
				}
			}
			else {
				if ((imRes.at<cv::Vec3b>(mapx, mapy)[0] == 0) &&
					(imRes.at<cv::Vec3b>(mapx, mapy)[1] == 0) &&
					(imRes.at<cv::Vec3b>(mapx, mapy)[2] == 0)) {
					for (int mapxOf = -1; mapxOf < 2; ++mapxOf) {
						for (int mapyOf = -1; mapyOf < 2; ++mapyOf) {
							int mapxSafe = (((mapx + mapxOf) > (imRes.rows - 2)) || ((mapx + mapxOf) < 0)) ? mapx : mapx + mapxOf;
							int mapySafe = (((mapy + mapyOf) > (imRes.cols - 2)) || ((mapy + mapyOf) < 0)) ? mapy : mapy + mapyOf;
							if ((imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[0] > 0) ||
								(imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[1] > 0) ||
								(imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[2] > 0)) {
								redMask += imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[0];
								greebMask += imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[1];
								blueMask += imRes.at<cv::Vec3b>(mapxSafe, mapySafe)[2];
								++cnt;
							}
						}
					}
					imRes.at<cv::Vec3b>(mapx, mapy)[0] = redMask / cnt;
					imRes.at<cv::Vec3b>(mapx, mapy)[1] = greebMask / cnt;
					imRes.at<cv::Vec3b>(mapx, mapy)[2] = blueMask / cnt;
				}
			}
		}
	}
	cv::imwrite(this->MyData.fNameMainResultPath + SclPath + SclPrefix + fName, imRes);
}