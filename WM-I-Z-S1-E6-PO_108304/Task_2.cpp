#include "Task_2.h"
#include <vector>
#include <algorithm>

cv::Task_2::Task_2()
{
	this->pxMinVal = 255;
	this->pxMaxVal = 0;

	// Adding 50 to the image
	this->fLoad();
	std::string AddingPath = "Task_2/Add/";
	std::string AddingPrefix = "AddV_";
	int32_t aValue = 50;
	this->imProc(&this->fGrImg1, this->add, aValue);
	this->imProc(&this->fGrImg2, this->add, aValue);
	this->imProc(&this->fRGBImg1, this->add, aValue);
	this->imProc(&this->fRGBImg2, this->add, aValue);
	this->imWriteAll(this->MyData.fNameMainResultPath + AddingPath, AddingPrefix);

	// Normalization after adding the value
	AddingPrefix = "Normalised_";
	this->imNorm(&this->fGrImg1);
	this->imNorm(&this->fGrImg2);
	this->imNorm(&this->fRGBImg1);
	this->imNorm(&this->fRGBImg2);
	this->imWriteAll(this->MyData.fNameMainResultPath + AddingPath, AddingPrefix);

	// Substract 50 from the image
	this->fLoad();
	std::string SubPath = "Task_2/Sub/";
	std::string SubPrefix = "SubV_";
	int32_t sValue = -50;
	this->imProc(&this->fGrImg1, this->add, sValue);
	this->imProc(&this->fGrImg2, this->add, sValue);
	this->imProc(&this->fRGBImg1, this->add, sValue);
	this->imProc(&this->fRGBImg2, this->add, sValue);
	this->imWriteAll(this->MyData.fNameMainResultPath + SubPath, SubPrefix);

	SubPrefix = "Normalised_";
	this->imNorm(&this->fGrImg1);
	this->imNorm(&this->fGrImg2);
	this->imNorm(&this->fRGBImg1);
	this->imNorm(&this->fRGBImg2);
	this->imWriteAll(this->MyData.fNameMainResultPath + SubPath, SubPrefix);

	// Multiply image and value
	this->fLoad();
	std::string MultPath = "Task_2/Mult/";
	std::string MultPrefix = "Mult_";
	this->imProc(&this->fGrImg1, this->mult, 2);
	this->imProc(&this->fGrImg2, this->mult, 2);
	this->imProc(&this->fRGBImg1, this->mult, 2);
	this->imProc(&this->fRGBImg2, this->mult, 2);
	this->imWriteAll(this->MyData.fNameMainResultPath + MultPath, MultPrefix);

	MultPrefix = "Normalised_";
	this->imNorm(&this->fGrImg1);
	this->imNorm(&this->fGrImg2);
	this->imNorm(&this->fRGBImg1);
	this->imNorm(&this->fRGBImg2);
	this->imWriteAll(this->MyData.fNameMainResultPath + MultPath, MultPrefix);

	// Summarise images
	this->fLoad();
	this->imSum(&this->fGrImg1, &this->fGrImg2);
	this->imSum(&this->fRGBImg1, &this->fRGBImg2);

	// Multiply images
	this->fLoad();
	this->imMultIm(&this->fGrImg1, &this->fGrImg2);
	this->imMultIm(&this->fRGBImg1, &this->fRGBImg2);

	// Overlay images
	this->fLoad();
	this->imOverlay(&this->fGrImg1, &this->fGrImg2);
	this->imOverlay(&this->fRGBImg1, &this->fRGBImg2);

	// Power images
	this->fLoad();
	std::string PowPath = "Task_2/Pow/";
	std::string PowPrefix = "Pow_";
	//this->imPow(&this->fGrImg1, 2);
	//this->imPow(&this->fGrImg2, 2);
	//this->imPow(&this->fRGBImg1, 2);
	//this->imPow(&this->fRGBImg2, 2);
	this->imProc(&this->fGrImg1, this->pw, 2);
	this->imProc(&this->fGrImg2, this->pw, 2);
	this->imProc(&this->fRGBImg1, this->pw, 2);
	this->imProc(&this->fRGBImg2, this->pw, 2);
	this->imWriteAll(this->MyData.fNameMainResultPath + PowPath, PowPrefix);
	this->imNorm(&this->fGrImg1);
	this->imNorm(&this->fGrImg2);
	this->imNorm(&this->fRGBImg1);
	this->imNorm(&this->fRGBImg2);
	PowPrefix = "Normalise_";
	this->imWriteAll(this->MyData.fNameMainResultPath + PowPath, PowPrefix);

	// Devide by value
	std::string DevPath = "Task_2/Dev/";
	std::string DevPrefix = "DevidedByValue_";
	this->imProc(&this->fGrImg1, this->dev, 2);
	this->imProc(&this->fGrImg2, this->dev, 2);
	this->imProc(&this->fRGBImg1, this->dev, 2);
	this->imProc(&this->fRGBImg2, this->dev, 2);
	this->imWriteAll(this->MyData.fNameMainResultPath + DevPath, DevPrefix);
	this->imNorm(&this->fGrImg1);
	this->imNorm(&this->fGrImg2);
	this->imNorm(&this->fRGBImg1);
	this->imNorm(&this->fRGBImg2);
	DevPrefix = "Normalised_";
	this->imWriteAll(this->MyData.fNameMainResultPath + DevPath, DevPrefix);

	// Devide image by image
	this->imDevByIm(&this->fGrImg1, &this->fGrImg2);
	this->imDevByIm(&this->fRGBImg1, &this->fRGBImg2);

	// Sqrt img
	this->fLoad();
	std::string SqrtPath = "Task_2/Sqrt/";
	std::string SqrtPrefix = "Sqrt_";
	this->imProc(&this->fGrImg1, this->mSqrt, 3);
	this->imProc(&this->fGrImg2, this->mSqrt, 3);
	this->imProc(&this->fRGBImg1, this->mSqrt, 3);
	this->imProc(&this->fRGBImg2, this->mSqrt, 3);
	this->imWriteAll(this->MyData.fNameMainResultPath + SqrtPath, SqrtPrefix);

	SqrtPrefix = "Normalised_";
	this->imNorm(&this->fGrImg1);
	this->imNorm(&this->fGrImg2);
	this->imNorm(&this->fRGBImg1);
	this->imNorm(&this->fRGBImg2);
	this->imWriteAll(this->MyData.fNameMainResultPath + SqrtPath, SqrtPrefix);

	// logarithm
	this->fLoad();
	std::string LogPath = "Task_2/Log/";
	std::string LogPrefix = "Log_";
	//this->imProc(&this->fGrImg1, this->mLog, 1);
	//this->imProc(&this->fGrImg2, this->mLog, 1);
	//this->imProc(&this->fRGBImg1, this->mLog, 1);
	//this->imProc(&this->fRGBImg2, this->mLog, 1);
	this->imLog(&this->fGrImg1);
	this->imLog(&this->fGrImg2);
	this->imLog(&this->fRGBImg1);
	this->imLog(&this->fRGBImg2);

	this->imWriteAll(this->MyData.fNameMainResultPath + LogPath, LogPrefix);

	LogPrefix = "Normalisation_";
	this->imNorm(&this->fGrImg1);
	this->imNorm(&this->fGrImg2);
	this->imNorm(&this->fRGBImg1);
	this->imNorm(&this->fRGBImg2);
	this->imWriteAll(this->MyData.fNameMainResultPath + LogPath, LogPrefix);
}

void cv::Task_2::fLoad()
{
	this->fGrImg1 = cv::imread(this->MyData.fNameSourcePath + this->MyData.fGreyName1, cv::IMREAD_GRAYSCALE);
	this->fGrImg2 = cv::imread(this->MyData.fNameSourcePath + this->MyData.fGreyName2, cv::IMREAD_GRAYSCALE);
	this->fRGBImg1 = cv::imread(this->MyData.fNameSourcePath + this->MyData.fRGBName1, cv::IMREAD_COLOR);
	this->fRGBImg2 = cv::imread(this->MyData.fNameSourcePath + this->MyData.fRGBName2, cv::IMREAD_COLOR);
}

void cv::Task_2::imProc(cv::Mat* tmpF, oprt oprt, int32_t value)
{
	double maxPxVal = 0;
	double minPxVal = 0;
	cv::minMaxLoc(*tmpF, &minPxVal, &maxPxVal);
	//this->pxMinMaxValue(tmpF);
	for (size_t mapx = 0; mapx < (*tmpF).cols; ++mapx) {
		for (size_t mapy = 0; mapy < (*tmpF).rows; ++mapy) {
			switch (oprt)
			{
			case(add):
				if ((*tmpF).channels() == 1) {
					if (tmpF->at<uchar>(mapx, mapy) + value > 255) {
						tmpF->at<uchar>(mapx, mapy) = 255;
					}
					else if (tmpF->at<uchar>(mapx, mapy) + value < 0) {
						tmpF->at<uchar>(mapx, mapy) = 0;
					}
					else {
						tmpF->at<uchar>(mapx, mapy) += value;
					}
				}
				else {
					for (size_t it = 0; it < (*tmpF).channels(); ++it) {
						if (tmpF->at<cv::Vec3b>(mapx, mapy)[it] + value > 255) {
							tmpF->at<cv::Vec3b>(mapx, mapy)[it] = 255;
						}
						else if (tmpF->at<cv::Vec3b>(mapx, mapy)[it] + value < 0) {
							tmpF->at<cv::Vec3b>(mapx, mapy)[it] = 0;
						}
						else {
							tmpF->at<cv::Vec3b>(mapx, mapy)[it] += value;
						}
					}
				}
				break;
			case(mult):
				if ((*tmpF).channels() == 1) {
					if (tmpF->at<uchar>(mapx, mapy) * value > 255) {
						tmpF->at<uchar>(mapx, mapy) = 255;
					}
					else if (tmpF->at<uchar>(mapx, mapy) * value < 0) {
						tmpF->at<uchar>(mapx, mapy) = 0;
					}
					else {
						tmpF->at<uchar>(mapx, mapy) *= value;
					}
				}
				else {
					for (size_t it = 0; it < (*tmpF).channels(); ++it) {
						if (tmpF->at<cv::Vec3b>(mapx, mapy)[it] * value > 255) {
							tmpF->at<cv::Vec3b>(mapx, mapy)[it] = 255;
						}
						else if (tmpF->at<cv::Vec3b>(mapx, mapy)[it] * value < 0) {
							tmpF->at<cv::Vec3b>(mapx, mapy)[it] = 0;
						}
						else {
							tmpF->at<cv::Vec3b>(mapx, mapy)[it] *= value;
						}
					}
				}
				break;
			case(pw):
				if ((*tmpF).channels() == 1) {
					tmpF->at<uchar>(mapx, mapy) = ceil(pow(tmpF->at<uchar>(mapx, mapy) / maxPxVal, value) * 255);
				}
				else {
					for (size_t it = 0; it < (*tmpF).channels(); ++it) {
						tmpF->at<cv::Vec3b>(mapx, mapy)[it] = ceil(pow(tmpF->at<cv::Vec3b>(mapx, mapy)[it] / maxPxVal, value) * 255);
					}
				}
				break;
			case(dev):
				this->pxMaxVal + value;
				if ((*tmpF).channels() == 1) {
					//int32_t tmpPx = ceil(((tmpF->at<uchar>(mapx, mapy) + value) * 255) / this->pxMaxVal);
					//tmpF->at<uchar>(mapx, mapy) = tmpPx;
					tmpF->at<uchar>(mapx, mapy) = ceil(tmpF->at<uchar>(mapx, mapy) / value);
					//cv::divide(value, *tmpF, *tmpF);
				}
				else {
					for (size_t it = 0; it < (*tmpF).channels(); ++it) {
						//tmpF->at<cv::Vec3b>(mapx, mapy)[it] = ceil(((tmpF->at<cv::Vec3b>(mapx, mapy)[it] + value) * 255) / this->pxMaxVal);
						tmpF->at<cv::Vec3b>(mapx, mapy)[it] = ceil(tmpF->at<cv::Vec3b>(mapx, mapy)[it] / value);
						//cv::divide(value, *tmpF, *tmpF);
					}
				}
			case(mSqrt):
			{
				if ((*tmpF).channels() == 1) {
					tmpF->at<uchar>(mapx, mapy) = ceil(pow(tmpF->at<uchar>(mapx, mapy) / maxPxVal, 1. / value) * 255);
				}
				else {
					for (size_t it = 0; it < (*tmpF).channels(); ++it) {
						tmpF->at<cv::Vec3b>(mapx, mapy)[it] = ceil(pow(tmpF->at<cv::Vec3b>(mapx, mapy)[it] / maxPxVal, 1. / value) * 255);
					}
				}
				//cv::pow(*tmpF, value, *tmpF);
				break;
			}
			//case(mLog):
			//{
			//	for (size_t mapx = 0; mapx < (*tmpF).cols; ++mapx) {
			//		for (size_t mapy = 0; mapy < (*tmpF).rows; ++mapy) {
			//			if ((*tmpF).channels() == 1) {
			//				if (tmpF->at<uchar>(mapx, mapy) != 0) {
			//					tmpF->at<uchar>(mapx, mapy) = ceil((std::log(1 + tmpF->at<uchar>(mapx, mapy)) / (std::log(1 + maxPxVal))) * 255);
			//				}
			//				if (tmpF->at<uchar>(mapx, mapy) == 0) tmpF->at<uchar>(mapx, mapy) = 0;
			//			}
			//			else {
			//				for (size_t it = 0; it < (*tmpF).channels(); ++it) {
			//					if (tmpF->at<cv::Vec3b>(mapx, mapy)[it] != 0) {
			//						tmpF->at<cv::Vec3b>(mapx, mapy)[it] = ceil((std::log(1 + tmpF->at<cv::Vec3b>(mapx, mapy)[it]) / (std::log(1 + maxPxVal))) * 255);
			//					}
			//					if (tmpF->at<cv::Vec3b>(mapx, mapy)[it] == 0) tmpF->at<cv::Vec3b>(mapx, mapy)[it] = 0;
			//				}
			//			}
			//		}
			//	}
			//	break;
			//}
			default:
				break;
			}
		}
	}
}

void cv::Task_2::pxMinMaxValue(cv::Mat* tmpF, cv::Mat* tmpV)
{
	for (size_t mapx = 0; mapx < (*tmpF).cols; ++mapx) {
		for (size_t mapy = 0; mapy < (*tmpF).rows; ++mapy) {
			if ((*tmpF).channels() == 1) {
				this->pxMinVal = (this->pxMinVal > (*tmpF).at<uchar>(mapx, mapy)) ? (*tmpF).at<uchar>(mapx, mapy) : this->pxMinVal;
				this->pxMaxVal = (this->pxMaxVal < (*tmpF).at<uchar>(mapx, mapy)) ? (*tmpF).at<uchar>(mapx, mapy) : this->pxMaxVal;
			}
			if ((*tmpF).channels() > 1) {
				for (size_t it = 0; it < (*tmpF).channels(); ++it) {
					this->pxMinVal = (this->pxMinVal > (*tmpF).at<cv::Vec3b>(mapx, mapy)[it]) ? (*tmpF).at<cv::Vec3b>(mapx, mapy)[it] : this->pxMinVal;
					this->pxMaxVal = (this->pxMaxVal < (*tmpF).at<cv::Vec3b>(mapx, mapy)[it]) ? (*tmpF).at<cv::Vec3b>(mapx, mapy)[it] : this->pxMaxVal;
				}
			}
		}
	}
}

void cv::Task_2::imNorm(cv::Mat* tmpF)
{
	//for (size_t mapx = 0; mapx < (*tmpF).cols; ++mapx) {
	//	for (size_t mapy = 0; mapy < (*tmpF).rows; ++mapy) {
	//		if ((*tmpF).channels() == 1) {
	//			if (255 * (tmpF->at<uchar>(mapx, mapy) - this->pxMinVal) / (this->pxMaxVal - this->pxMinVal) < 0) {
	//				tmpF->at<uchar>(mapx, mapy) = 3;
	//			}
	//			else {
	//				tmpF->at<uchar>(mapx, mapy) = 255 * (tmpF->at<uchar>(mapx, mapy) - this->pxMinVal) / (this->pxMaxVal - this->pxMinVal);
	//			}
	//		}
	//		else {
	//			for (size_t it = 0; it < (*tmpF).channels(); ++it) {
	//				if (255 * (tmpF->at<cv::Vec3b>(mapx, mapy)[it] - this->pxMinVal) / (this->pxMaxVal - this->pxMinVal) < 0) {
	//					tmpF->at<cv::Vec3b>(mapx, mapy)[it] = 3;
	//				}
	//				else {
	//					tmpF->at<cv::Vec3b>(mapx, mapy)[it] = 255 * (tmpF->at<cv::Vec3b>(mapx, mapy)[it] - this->pxMinVal) / (this->pxMaxVal - this->pxMinVal);
	//				}
	//			}
	//		}
	//	}
	//}
	cv::normalize(*tmpF, *tmpF, 0, 255, cv::NORM_MINMAX);
}

void cv::Task_2::imWriteAll(std::string fPath, std::string fPrefix)
{
	cv::imwrite(fPath + fPrefix + this->MyData.fGreyName1, this->fGrImg1);
	cv::imwrite(fPath + fPrefix + this->MyData.fGreyName2, this->fGrImg2);
	cv::imwrite(fPath + fPrefix + this->MyData.fRGBName1, this->fRGBImg1);
	cv::imwrite(fPath + fPrefix + this->MyData.fRGBName2, this->fRGBImg2);
}

void cv::Task_2::imSum(cv::Mat* tmpF1, cv::Mat* tmpF2)
{
	int32_t startPx = (tmpF1->cols > tmpF2->cols) ? (tmpF1->cols - tmpF2->cols) / 2 : (tmpF2->cols - tmpF1->cols) / 2;
	int32_t startPy = (tmpF1->rows > tmpF2->rows) ? (tmpF1->rows - tmpF2->rows) / 2 : (tmpF2->rows - tmpF1->rows) / 2;
	int32_t tmpMaxWidth = (tmpF1->cols > tmpF2->cols) ? tmpF1->cols : tmpF2->cols;
	int32_t tmpMaxHeight = (tmpF1->rows > tmpF2->rows) ? tmpF1->rows : tmpF2->rows;
	cv::Mat ResIm(tmpMaxWidth, tmpMaxHeight, tmpF1->type(), cv::Scalar(0,0,0));

	double mScope1 = 0;
	double mScope2 = 0;
	double anyV = 0;
	cv::minMaxLoc(*tmpF1, &anyV, &mScope1);
	cv::minMaxLoc(*tmpF2, &anyV, &mScope2);
	int32_t mScope = ceil(mScope1 + mScope2);

	mScope = (mScope > 255) ? ((mScope - 255) / 255) : mScope;

	cv::resize(*tmpF1, *tmpF1, cv::Size(tmpMaxWidth, tmpMaxHeight));
	cv::resize(*tmpF2, *tmpF2, cv::Size(tmpMaxWidth, tmpMaxHeight));

	// First try
	//for (size_t mapx = 0; mapx < tmpMaxWidth; ++mapx) {
	//	for (size_t mapy = 0; mapy < tmpMaxHeight; ++mapy) {
	//		if (tmpF1->channels() == 1 && tmpF1->type() == tmpF2->type()) {
	//			//ResIm.at<uchar>(mapx, mapy) = (tmpF1->at<uchar>(mapx, mapy) - tmpF1->at<uchar>(mapx, mapy) * mScope) + (tmpF2->at<uchar>(mapx, mapy) - tmpF2->at<uchar>(mapx, mapy) * mScope);
	//			//ResIm.at<uchar>(mapx, mapy) = ((tmpF1->at<uchar>(mapx, mapy) - tmpF1->at<uchar>(mapx, mapy) * mScope) > (tmpF2->at<uchar>(mapx, mapy) - tmpF2->at<uchar>(mapx, mapy) * mScope)) ?
	//			//	(tmpF1->at<uchar>(mapx, mapy) - tmpF1->at<uchar>(mapx, mapy) * mScope) : (tmpF2->at<uchar>(mapx, mapy) - tmpF2->at<uchar>(mapx, mapy) * mScope);
	//			ResIm.at<uchar>(mapx, mapy) = (tmpF1->at<uchar>(mapx, mapy) + tmpF2->at<uchar>(mapx, mapy)) / 255;
	//		}
	//		else {
	//			for (size_t it = 0; it < tmpF1->channels(); ++it) {
	//				//ResIm.at<cv::Vec3b>(mapx, mapy)[it] = (tmpF1->at<cv::Vec3b>(mapx, mapy)[it] - tmpF1->at<cv::Vec3b>(mapx, mapy)[it] * mScope) +
	//				//	(tmpF2->at<cv::Vec3b>(mapx, mapy)[it] - (tmpF2->at<cv::Vec3b>(mapx, mapy)[it] * mScope));
	//				//ResIm.at<cv::Vec3b>(mapx, mapy)[it] = ((tmpF1->at<cv::Vec3b>(mapx, mapy)[it] - tmpF1->at<cv::Vec3b>(mapx, mapy)[it] * mScope) >
	//				//	(tmpF2->at<cv::Vec3b>(mapx, mapy)[it] - (tmpF2->at<cv::Vec3b>(mapx, mapy)[it] * mScope))) ?
	//				//	(tmpF1->at<cv::Vec3b>(mapx, mapy)[it] - tmpF1->at<cv::Vec3b>(mapx, mapy)[it] * mScope) :
	//				//	(tmpF2->at<cv::Vec3b>(mapx, mapy)[it] - (tmpF2->at<cv::Vec3b>(mapx, mapy)[it] * mScope));
	//				ResIm.at<cv::Vec3b>(mapx, mapy)[it] = (tmpF1->at<cv::Vec3b>(mapx, mapy)[it] + tmpF2->at<cv::Vec3b>(mapx, mapy)[it]) / 255;
	//			}
	//		}
	//	}
	//}

	// Second try (doesn't work)
	//std::vector<cv::Mat> channels;
	//channels.push_back(*tmpF2);
	//channels.push_back(*tmpF1);
	//channels.push_back(*tmpF2);
	//std::merge(&channels[0], channels.size(), ResIm);

	// Third try (Doesn't work)
	//cv::addWeighted(*tmpF1, 0.2, *tmpF2, 0.2, 0.0, ResIm);
	cv::add(*tmpF1, *tmpF2, ResIm);

	std::string SumPath = "Task_2/Sum/";
	std::string SumPrefix = "Sum_";
	//if (tmpF1->channels() == 1) {
	//	std::cout << this->MyData.fNameMainResultPath + SumPath + SumPrefix + "GreyImgSum" << std::endl;
	//	cv::imwrite(this->MyData.fNameMainResultPath + SumPath + SumPrefix + "GreyImgSum", ResIm);
	//	SumPrefix = "Normalised_";
	//	this->imNorm(&ResIm);
	//	cv::imwrite(this->MyData.fNameMainResultPath + SumPath + SumPrefix + "GreyImgSum", ResIm);
	//}
	//else {
	//	cv::imwrite(this->MyData.fNameMainResultPath + SumPath + SumPrefix + "RGBImgSum", ResIm);
	//	SumPrefix = "Normalised_";
	//	this->imNorm(&ResIm);
	//	cv::imwrite(this->MyData.fNameMainResultPath + SumPath + SumPrefix + "RGBImgSum", ResIm);
	//}
	cv::imwrite(this->MyData.fNameMainResultPath + SumPath + SumPrefix + ((tmpF1->channels() == 1 && tmpF1->type() == tmpF2->type()) ? "GreyImgSum.bmp" : "RGBImgSum.bmp"), ResIm);

	SumPrefix = "Normalised_";
	this->imNorm(&ResIm);
	cv::imwrite(this->MyData.fNameMainResultPath + SumPath + SumPrefix + ((tmpF1->channels() == 1 && tmpF1->type() == tmpF2->type()) ? "GreyImgSum.bmp" : "RGBImgSum.bmp"), ResIm);

}

void cv::Task_2::imMultIm(cv::Mat* tmpF1, cv::Mat* tmpF2)
{
	int32_t tmpMaxWidth = (tmpF1->cols > tmpF2->cols) ? tmpF1->cols : tmpF2->cols;
	int32_t tmpMaxHeight = (tmpF1->rows > tmpF2->rows) ? tmpF1->rows : tmpF2->rows;
	cv::Mat ResIm(tmpMaxWidth, tmpMaxHeight, tmpF1->type(), cv::Scalar(0, 0, 0));

	cv::resize(*tmpF1, *tmpF1, cv::Size(tmpMaxWidth, tmpMaxHeight));
	cv::resize(*tmpF2, *tmpF2, cv::Size(tmpMaxWidth, tmpMaxHeight));

	for (size_t mapx = 0; mapx < tmpMaxWidth; ++mapx) {
		for (size_t mapy = 0; mapy < tmpMaxHeight; ++mapy) {
			if ((*tmpF1).channels() == 1) {
				ResIm.at<uchar>(mapx, mapy) = (tmpF1->at<uchar>(mapx, mapy) * tmpF2->at<uchar>(mapx, mapy)) / 255;
			}
			else {
				for (size_t it = 0; it < tmpF1->channels(); ++it) {
					ResIm.at<cv::Vec3b>(mapx, mapy)[it] = (tmpF1->at<cv::Vec3b>(mapx, mapy)[it] * tmpF2->at<cv::Vec3b>(mapx, mapy)[it]) / 255;
				}
			}
		}
	}

	std::string MultImPath = "Task_2/MultIm/";
	std::string MultImPrefix = "MultIm_";

	cv::imwrite(this->MyData.fNameMainResultPath + MultImPath + MultImPrefix + ((tmpF1->channels() == 1 && tmpF1->type() == tmpF2->type()) ? "GreyImgMult.bmp" : "RGBImgMult.bmp"), ResIm);

	MultImPrefix = "Normalised_";
	this->imNorm(&ResIm);
	cv::imwrite(this->MyData.fNameMainResultPath + MultImPath + MultImPrefix + ((tmpF1->channels() == 1 && tmpF1->type() == tmpF2->type()) ? "GreyImgMult.bmp" : "RGBImgMult.bmp"), ResIm);

}

void cv::Task_2::imOverlay(cv::Mat* tmpF1, cv::Mat* tmpF2)
{
	int32_t tmpMaxWidth = (tmpF1->cols > tmpF2->cols) ? tmpF1->cols : tmpF2->cols;
	int32_t tmpMaxHeight = (tmpF1->rows > tmpF2->rows) ? tmpF1->rows : tmpF2->rows;
	cv::Mat ResIm(tmpMaxWidth, tmpMaxHeight, tmpF1->type(), cv::Scalar(0, 0, 0));

	cv::resize(*tmpF1, *tmpF1, cv::Size(tmpMaxWidth, tmpMaxHeight));
	cv::resize(*tmpF2, *tmpF2, cv::Size(tmpMaxWidth, tmpMaxHeight));

	std::string OverlayPath = "Task_2/Overlay/";
	std::string OverlayPrefix = "Overlay_";

	cv::addWeighted(*tmpF1, 0.3, *tmpF2, 0.7, 0.0, ResIm);

	cv::imwrite(this->MyData.fNameMainResultPath + OverlayPath + OverlayPrefix + ((tmpF1->channels() == 1 && tmpF1->type() == tmpF2->type()) ? "GreyImgOverlay.bmp" : "RGBImgOverlay.bmp"), ResIm);

	OverlayPrefix = "Normalised_";
	this->imNorm(&ResIm);
	cv::imwrite(this->MyData.fNameMainResultPath + OverlayPath + OverlayPrefix + ((tmpF1->channels() == 1 && tmpF1->type() == tmpF2->type()) ? "GreyImgOverlay.bmp" : "RGBImgOverlay.bmp"), ResIm);
}

void cv::Task_2::imDevByIm(cv::Mat* tmpF1, cv::Mat* tmpF2)
{
	int32_t tmpMaxWidth = (tmpF1->cols > tmpF2->cols) ? tmpF1->cols : tmpF2->cols;
	int32_t tmpMaxHeight = (tmpF1->rows > tmpF2->rows) ? tmpF1->rows : tmpF2->rows;
	int32_t maxPxValue = 0;
	cv::Mat ResIm(tmpMaxWidth, tmpMaxHeight, tmpF1->type(), cv::Scalar(0, 0, 0));

	cv::resize(*tmpF1, *tmpF1, cv::Size(tmpMaxWidth, tmpMaxHeight));
	cv::resize(*tmpF2, *tmpF2, cv::Size(tmpMaxWidth, tmpMaxHeight));

	for (size_t mapx = 0; mapx < tmpMaxWidth; ++mapx) {
		for (size_t mapy = 0; mapy < tmpMaxHeight; ++mapy) {
			if ((*tmpF1).channels() == 1) {
				if (maxPxValue < (int32_t)(tmpF1->at<uchar>(mapx, mapy) + tmpF2->at<uchar>(mapx, mapx)))
					maxPxValue = (int32_t)(tmpF1->at<uchar>(mapx, mapy) + tmpF2->at<uchar>(mapx, mapx));
			}
			else {
				for (size_t it = 0; it < tmpF1->channels(); ++it) {
					if (maxPxValue < (int32_t)(tmpF1->at<cv::Vec3b>(mapx, mapy)[it] + tmpF2->at<cv::Vec3b>(mapx, mapy)[it]))
						maxPxValue = (int32_t)(tmpF1->at<cv::Vec3b>(mapx, mapy)[it] + tmpF2->at<cv::Vec3b>(mapx, mapy)[it]);
				}
			}
		}
	}

	for (size_t mapx = 0; mapx < tmpMaxWidth; ++mapx) {
		for (size_t mapy = 0; mapy < tmpMaxHeight; ++mapy) {
			if ((*tmpF1).channels() == 1) {
				ResIm.at<uchar>(mapx, mapy) = ceil(((tmpF1->at<uchar>(mapx, mapy) + tmpF2->at<uchar>(mapx, mapy)) * 255) / maxPxValue);
			}
			else {
				for (size_t it = 0; it < tmpF1->channels(); ++it) {
					ResIm.at<cv::Vec3b>(mapx, mapy)[it] = ceil(((tmpF1->at<cv::Vec3b>(mapx, mapy)[it] + tmpF2->at<cv::Vec3b>(mapx, mapy)[it]) * 255) / maxPxValue);
				}
			}
		}
	}

	//cv::divide(*tmpF1, *tmpF2, ResIm);

	std::string DevImPath = "Task_2/DevImByIm/";
	std::string DevImPrefix = "DevImByIm_";

	cv::imwrite(this->MyData.fNameMainResultPath + DevImPath + DevImPrefix + ((tmpF1->channels() == 1 && tmpF1->type() == tmpF2->type()) ? "GreyImgOverlay.bmp" : "RGBImgOverlay.bmp"), ResIm);

	DevImPrefix = "Normalised_";
	this->imNorm(&ResIm);
	cv::imwrite(this->MyData.fNameMainResultPath + DevImPath + DevImPrefix + ((tmpF1->channels() == 1 && tmpF1->type() == tmpF2->type()) ? "GreyImgOverlay.bmp" : "RGBImgOverlay.bmp"), ResIm);
}

void cv::Task_2::imLog(cv::Mat* tmpF)
{
	double maxPxVal = 0;
	double minPxVal = 255;
	cv::minMaxLoc(*tmpF, &minPxVal, &maxPxVal);
	for (size_t mapx = 0; mapx < (*tmpF).cols; ++mapx) {
		for (size_t mapy = 0; mapy < (*tmpF).rows; ++mapy) {
			if ((*tmpF).channels() == 1) {
				if (tmpF->at<uchar>(mapx, mapy) != 0) {
					tmpF->at<uchar>(mapx, mapy) = ceil((std::log(1 + tmpF->at<uchar>(mapx, mapy)) / (std::log(1 + maxPxVal))) * 255);
				}
				if (tmpF->at<uchar>(mapx, mapy) == 0) tmpF->at<uchar>(mapx, mapy) = 0;
			}
			else {
				for (size_t it = 0; it < (*tmpF).channels(); ++it) {
					if (tmpF->at<cv::Vec3b>(mapx, mapy)[it] != 0) {
						tmpF->at<cv::Vec3b>(mapx, mapy)[it] = ceil((std::log(1 + tmpF->at<cv::Vec3b>(mapx, mapy)[it]) / (std::log(1 + maxPxVal))) * 255);
					}
					if (tmpF->at<cv::Vec3b>(mapx, mapy)[it] == 0) tmpF->at<cv::Vec3b>(mapx, mapy)[it] = 0;
				}
			}
		}
	}
}