#include "MyData.h"

cv::MyData::MyData()
{
	// Source images path
	this->fNameSourcePath = "../source/";
	// Result images path
	this->fNameMainResultPath = "../result/";
	// Test file names
	this->fGreyName1 = "grimg1.bmp";
	this->fGreyName2 = "grimg2.bmp";
	this->fRGBName1 = "rgbimg1.bmp";
	this->fRGBName2 = "rgbimg2.bmp";
}