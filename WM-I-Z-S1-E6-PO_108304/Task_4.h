#pragma once

#include "MyData.h"

namespace cv
{
	class Task_4
	{
		cv::MyData MyData;
		cv::Mat fGrImg1;
		cv::Mat fGrImg2;
		cv::Mat fRGBImg1;
		cv::Mat fRGBImg2;
		enum class MySymmetry
		{
			x_axis,
			y_axis,
			mx_axis,
			my_axis
		};

	public:
		Task_4();
		void fLoad();
		void rmFragment(cv::Mat tmpF, std::string fName, int width, int height, int posx = 0, int posy = 0);
		void copyFragment(cv::Mat tmpF, std::string fName, int width, int height, int posx = 0, int posy = 0);
		void moveByVector(cv::Mat tmpF, std::string fName, int posx, int posy);
		void imSymmetry(cv::Mat tmpF, std::string fName, MySymmetry axis);
		void imRotate(cv::Mat tmpF, std::string fName, int myAngle);
		void homoScal(cv::Mat tmpF, std::string fName, int scalValx = 1, int scalValy = 1);
	};
} // cv namespace end