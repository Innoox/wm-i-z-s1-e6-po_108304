#include "ShowResult.h"

cv::ShowResult::ShowResult()
{
	this->SourcePath = "../source/";
	this->ResultPath = "../result/";

	this->fGreyName1 = "grimg1.bmp";
	this->fGreyName2 = "grimg2.bmp";
	this->fRGBName1 = "rgbimg1.bmp";
	this->fRGBName2 = "rgbimg2.bmp";

	this->fLoad();
}

void cv::ShowResult::fLoad()
{
	this->fGrImg1 = cv::imread(this->SourcePath + this->fGreyName1, cv::IMREAD_GRAYSCALE);
	this->fGrImg2 = cv::imread(this->SourcePath + this->fGreyName2, cv::IMREAD_GRAYSCALE);
	this->fRGBImg1 = cv::imread(this->SourcePath + this->fRGBName1, cv::IMREAD_COLOR);
	this->fRGBImg2 = cv::imread(this->SourcePath + this->fRGBName2, cv::IMREAD_COLOR);
}

void cv::ShowResult::ShowResultT_1_1()
{
	this->CurrentTaskPath = "Task_1/GeomResize/";
	this->fName1 = "GeomResize_grimg1.bmp";
	this->fName2 = "GeomResize_grimg2.bmp";
	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_GRAYSCALE);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
}

void cv::ShowResult::ShowResultT_1_2()
{
	this->CurrentTaskPath = "Task_1/RasterResize/";
	this->fName1 = "RasterResize_grimg1.bmp";
	this->fName2 = "RasterResize_grimg2.bmp";
	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_GRAYSCALE);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
}

void cv::ShowResult::ShowResultT_1_3()
{
	this->CurrentTaskPath = "Task_1/GeomResize/";
	this->fName1 = "GeomResize_rgbimg1.bmp";
	this->fName2 = "GeomResize_rgbimg2.bmp";
	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_COLOR);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_COLOR);

	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fGreyName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::waitKey('27');
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
}

void cv::ShowResult::ShowResultT_1_4()
{
	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	this->CurrentTaskPath = "Task_1/RasterResize/";
	this->fName1 = "RasterResize_rgbimg1.bmp";
	this->fName2 = "RasterResize_rgbimg2.bmp";
	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_COLOR);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_COLOR);

	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fGreyName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::waitKey('27');
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
}

void cv::ShowResult::ShowResultT_2_1()
{
	this->CurrentTaskPath = "Task_2/Add/";
	this->fName1 = "AddV_grimg1.bmp";
	this->fName2 = "AddV_grimg2.bmp";
	this->fNameN1 = "Normalised_grimg1.bmp";
	this->fNameN2 = "Normalised_grimg2.bmp";

	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_GRAYSCALE);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
}

void cv::ShowResult::ShowResultT_2_2()
{
	this->CurrentTaskPath = "Task_2/Mult/";
	this->fName1 = "Mult_grimg1.bmp";
	this->fName2 = "Mult_grimg2.bmp";
	this->fNameN1 = "Normalised_grimg1.bmp";
	this->fNameN2 = "Normalised_grimg2.bmp";

	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_GRAYSCALE);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
}

void cv::ShowResult::ShowResultT_2_3()
{
	this->CurrentTaskPath = "Task_2/Overlay/";
	this->fName1 = "Overlay_GreyImgOverlay.bmp";
	this->fNameN1 = "Normalised_GreyImgOverlay.bmp";

	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_GRAYSCALE);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fNameN1, myResN1);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fNameN1);
}

void cv::ShowResult::ShowResultT_2_4()
{
	this->CurrentTaskPath = "Task_2/Pow/";
	this->fName1 = "Pow_grimg1.bmp";
	this->fName2 = "Pow_grimg2.bmp";
	this->fNameN1 = "Normalise_grimg1.bmp";
	this->fNameN2 = "Normalise_grimg2.bmp";

	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_GRAYSCALE);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
}

void cv::ShowResult::ShowResultT_2_5()
{
	this->CurrentTaskPath = "Task_2/Dev/";
	this->fName1 = "DevidedByValue_grimg1.bmp";
	this->fName2 = "DevidedByValue_grimg2.bmp";
	this->fNameN1 = "Normalised_grimg1.bmp";
	this->fNameN2 = "Normalised_grimg2.bmp";

	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_GRAYSCALE);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
}

void cv::ShowResult::ShowResultT_2_6()
{
	this->CurrentTaskPath = "Task_2/Sqrt/";
	this->fName1 = "Sqrt_grimg1.bmp";
	this->fName2 = "Sqrt_grimg2.bmp";
	this->fNameN1 = "Normalised_grimg1.bmp";
	this->fNameN2 = "Normalised_grimg2.bmp";

	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_GRAYSCALE);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
}

void cv::ShowResult::ShowResultT_2_7()
{
	this->CurrentTaskPath = "Task_2/Log/";
	this->fName1 = "Log_grimg1.bmp";
	this->fName2 = "Log_grimg2.bmp";
	this->fNameN1 = "Normalisation_grimg1.bmp";
	this->fNameN2 = "Normalisation_grimg2.bmp";

	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_GRAYSCALE);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
}

void cv::ShowResult::ShowResultT_2_8()
{
	this->CurrentTaskPath = "Task_2/Sum/";
	this->fName1 = "Sum_GreyImgSum.bmp";
	this->fNameN1 = "Normalised_GreyImgSum.bmp";

	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_GRAYSCALE);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fNameN1, myResN1);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fNameN1);
}

void cv::ShowResult::ShowResultT_2_9()
{
	this->CurrentTaskPath = "Task_2/MultIm/";
	this->fName1 = "MultIm_GreyImgMult.bmp";
	this->fNameN1 = "Normalised_GreyImgMult.bmp";

	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_GRAYSCALE);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fNameN1, myResN1);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fNameN1);
}

void cv::ShowResult::ShowResultT_2_10()
{
	this->CurrentTaskPath = "Task_2/DevImByIm/";
	this->fName1 = "DevImByIm_GreyImgOverlay.bmp";
	this->fNameN1 = "Normalised_GreyImgOverlay.bmp";

	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_GRAYSCALE);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fNameN1, myResN1);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fNameN1);
}

void cv::ShowResult::ShowResultT_3_1()
{
	this->CurrentTaskPath = "Task_2/Add/";
	this->fName1 = "AddV_rgbimg1.bmp";
	this->fName2 = "AddV_rgbimg2.bmp";
	this->fNameN1 = "Normalised_rgbimg1.bmp";
	this->fNameN2 = "Normalised_rgbimg2.bmp";
	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_COLOR);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_COLOR);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_COLOR);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_COLOR);

	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::waitKey('27');
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
}

void cv::ShowResult::ShowResultT_3_2()
{
	this->CurrentTaskPath = "Task_2/Mult/";
	this->fName1 = "Mult_rgbimg1.bmp";
	this->fName2 = "Mult_rgbimg2.bmp";
	this->fNameN1 = "Normalised_rgbimg1.bmp";
	this->fNameN2 = "Normalised_rgbimg2.bmp";
	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_COLOR);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_COLOR);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_COLOR);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_COLOR);

	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::waitKey('27');
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
}

void cv::ShowResult::ShowResultT_3_3()
{
	this->CurrentTaskPath = "Task_2/Overlay/";
	this->fName1 = "Overlay_RGBImgOverlay.bmp";
	this->fNameN1 = "Normalised_RGBImgOverlay.bmp";
	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_COLOR);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_COLOR);

	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fNameN1, myResN1);
	cv::waitKey('27');
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fNameN1);
}

void cv::ShowResult::ShowResultT_3_4()
{
	this->CurrentTaskPath = "Task_2/Mult/";
	this->fName1 = "Pow_rgbimg1.bmp";
	this->fName2 = "Pow_rgbimg2.bmp";
	this->fNameN1 = "Normalise_rgbimg1.bmp";
	this->fNameN2 = "Normalise_rgbimg2.bmp";
	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_COLOR);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_COLOR);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_COLOR);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_COLOR);

	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::waitKey('27');
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
}

void cv::ShowResult::ShowResultT_3_5()
{
	this->CurrentTaskPath = "Task_2/Dev/";
	this->fName1 = "DevidedByValue_rgbimg1.bmp";
	this->fName2 = "DevidedByValue_rgbimg2.bmp";
	this->fNameN1 = "Normalised_rgbimg1.bmp";
	this->fNameN2 = "Normalised_rgbimg2.bmp";
	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_COLOR);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_COLOR);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_COLOR);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_COLOR);

	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::waitKey('27');
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
}

void cv::ShowResult::ShowResultT_3_6()
{
	this->CurrentTaskPath = "Task_2/Sqrt/";
	this->fName1 = "Sqrt_rgbimg1.bmp";
	this->fName2 = "Sqrt_rgbimg2.bmp";
	this->fNameN1 = "Normalised_rgbimg1.bmp";
	this->fNameN2 = "Normalised_rgbimg2.bmp";
	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_COLOR);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_COLOR);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_COLOR);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_COLOR);

	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::waitKey('27');
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
}

void cv::ShowResult::ShowResultT_3_7()
{
	this->CurrentTaskPath = "Task_2/Log/";
	this->fName1 = "Log_rgbimg1.bmp";
	this->fName2 = "Log_rgbimg2.bmp";
	this->fNameN1 = "Normalisation_rgbimg1.bmp";
	this->fNameN2 = "Normalisation_rgbimg2.bmp";
	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_COLOR);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_COLOR);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_COLOR);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_COLOR);

	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::waitKey('27');
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
}

void cv::ShowResult::ShowResultT_3_8()
{
	this->CurrentTaskPath = "Task_2/Sum/";
	this->fName1 = "Sum_RGBImgSum.bmp";
	this->fNameN1 = "Normalised_RGBImgSum.bmp";
	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_COLOR);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_COLOR);

	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fNameN1, myResN1);
	cv::waitKey('27');
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fNameN1);
}

void cv::ShowResult::ShowResultT_3_9()
{
	this->CurrentTaskPath = "Task_2/MultIm/";
	this->fName1 = "MultIm_RGBImgMult.bmp";
	this->fNameN1 = "Normalised_RGBImgMult.bmp";
	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_COLOR);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_COLOR);

	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fNameN1, myResN1);
	cv::waitKey('27');
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fNameN1);
}

void cv::ShowResult::ShowResultT_3_10()
{
	this->CurrentTaskPath = "Task_2/DevImByIm/";
	this->fName1 = "DevImByIm_RGBImgOverlay.bmp";
	this->fNameN1 = "Normalised_RGBImgOverlay.bmp";
	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_COLOR);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_COLOR);

	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fNameN1, myResN1);
	cv::waitKey('27');
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fNameN1);
}

void cv::ShowResult::ShowResultT_4_1()
{
	this->CurrentTaskPath = "Task_4/MoveFragment/";
	this->fName1 = "Moved_grimg1.bmp";
	this->fName2 = "Moved_grimg2.bmp";
	this->fName3 = "Moved_rgbimg1.bmp";
	this->fName4 = "Moved_rgbimg2.bmp";

	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes3 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName3, cv::IMREAD_COLOR);
	cv::Mat myRes4 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName4, cv::IMREAD_COLOR);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fName3, myRes3);
	cv::imshow(this->fName4, myRes4);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fName3);
	cv::destroyWindow(this->fName4);
}

void cv::ShowResult::ShowResultT_4_2()
{
	this->CurrentTaskPath = "Task_4/Scale/";
	this->fName1 = "grimg1.bmp";
	this->fName2 = "grimg2.bmp";
	this->fName3 = "rgbimg1.bmp";
	this->fName4 = "rgbimg2.bmp";

	this->fNameN1 = "Inrepolation_grimg1.bmp";
	this->fNameN2 = "Inrepolation_grimg2.bmp";
	this->fNameN3 = "Inrepolation_rgbimg1.bmp";
	this->fNameN4 = "Inrepolation_rgbimg2.bmp";


	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes3 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName3, cv::IMREAD_COLOR);
	cv::Mat myRes4 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName4, cv::IMREAD_COLOR);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN3 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN3, cv::IMREAD_COLOR);
	cv::Mat myResN4 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN4, cv::IMREAD_COLOR);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fName3, myRes3);
	cv::imshow(this->fName4, myRes4);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::imshow(this->fNameN3, myResN3);
	cv::imshow(this->fNameN4, myResN4);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fName3);
	cv::destroyWindow(this->fName4);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
	cv::destroyWindow(this->fNameN3);
	cv::destroyWindow(this->fNameN4);
}

void cv::ShowResult::ShowResultT_4_3()
{
	this->CurrentTaskPath = "Task_4/ScaleUnEqual/";
	this->fName1 = "grimg1.bmp";
	this->fName2 = "grimg2.bmp";
	this->fName3 = "rgbimg1.bmp";
	this->fName4 = "rgbimg2.bmp";

	this->fNameN1 = "Inrepolation_grimg1.bmp";
	this->fNameN2 = "Inrepolation_grimg2.bmp";
	this->fNameN3 = "Inrepolation_rgbimg1.bmp";
	this->fNameN4 = "Inrepolation_rgbimg2.bmp";


	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes3 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName3, cv::IMREAD_COLOR);
	cv::Mat myRes4 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName4, cv::IMREAD_COLOR);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN3 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN3, cv::IMREAD_COLOR);
	cv::Mat myResN4 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN4, cv::IMREAD_COLOR);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fName3, myRes3);
	cv::imshow(this->fName4, myRes4);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::imshow(this->fNameN3, myResN3);
	cv::imshow(this->fNameN4, myResN4);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fName3);
	cv::destroyWindow(this->fName4);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
	cv::destroyWindow(this->fNameN3);
	cv::destroyWindow(this->fNameN4);
}

void cv::ShowResult::ShowResultT_4_4()
{
	this->CurrentTaskPath = "Task_4/Rotation/";
	this->fName1 = "grimg1.bmp";
	this->fName2 = "grimg2.bmp";
	this->fName3 = "Interpolation_grimg1.bmp";
	this->fName4 = "Interpolation_grimg2.bmp";

	this->fNameN1 = "rgbimg1.bmp";
	this->fNameN2 = "rgbimg2.bmp";
	this->fNameN3 = "Interpolation_rgbimg1.bmp";
	this->fNameN4 = "Interpolation_rgbimg2.bmp";


	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes3 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName3, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes4 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName4, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_COLOR);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_COLOR);
	cv::Mat myResN3 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN3, cv::IMREAD_COLOR);
	cv::Mat myResN4 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN4, cv::IMREAD_COLOR);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fName3, myRes3);
	cv::imshow(this->fName4, myRes4);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::imshow(this->fNameN3, myResN3);
	cv::imshow(this->fNameN4, myResN4);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fName3);
	cv::destroyWindow(this->fName4);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
	cv::destroyWindow(this->fNameN3);
	cv::destroyWindow(this->fNameN4);
}

void cv::ShowResult::ShowResultT_4_5()
{
	this->CurrentTaskPath = "Task_4/Symmetry/";
	this->fName1 = "ByX_grimg1.bmp";
	this->fName2 = "ByY_grimg1.bmp";
	this->fName3 = "ByX_grimg2.bmp";
	this->fName4 = "ByY_grimg2.bmp";

	this->fNameN1 = "ByX_rgbimg1.bmp";
	this->fNameN2 = "ByY_rgbimg1.bmp";
	this->fNameN3 = "ByX_rgbimg2.bmp";
	this->fNameN4 = "ByY_rgbimg2.bmp";


	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes3 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName3, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes4 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName4, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_COLOR);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_COLOR);
	cv::Mat myResN3 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN3, cv::IMREAD_COLOR);
	cv::Mat myResN4 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN4, cv::IMREAD_COLOR);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fName3, myRes3);
	cv::imshow(this->fName4, myRes4);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::imshow(this->fNameN3, myResN3);
	cv::imshow(this->fNameN4, myResN4);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fName3);
	cv::destroyWindow(this->fName4);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
	cv::destroyWindow(this->fNameN3);
	cv::destroyWindow(this->fNameN4);
}

void cv::ShowResult::ShowResultT_4_6()
{
	this->CurrentTaskPath = "Task_4/SymmetryMiddle/";
	this->fName1 = "ByMX_grimg1.bmp";
	this->fName2 = "ByMY_grimg1.bmp";
	this->fName3 = "ByMX_grimg2.bmp";
	this->fName4 = "ByMY_grimg2.bmp";

	this->fNameN1 = "ByMX_rgbimg1.bmp";
	this->fNameN2 = "ByMY_rgbimg1.bmp";
	this->fNameN3 = "ByMX_rgbimg2.bmp";
	this->fNameN4 = "ByMY_rgbimg2.bmp";


	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes3 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName3, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes4 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName4, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_COLOR);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_COLOR);
	cv::Mat myResN3 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN3, cv::IMREAD_COLOR);
	cv::Mat myResN4 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN4, cv::IMREAD_COLOR);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fName3, myRes3);
	cv::imshow(this->fName4, myRes4);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::imshow(this->fNameN3, myResN3);
	cv::imshow(this->fNameN4, myResN4);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fName3);
	cv::destroyWindow(this->fName4);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
	cv::destroyWindow(this->fNameN3);
	cv::destroyWindow(this->fNameN4);
}

void cv::ShowResult::ShowResultT_4_7()
{
	this->CurrentTaskPath = "Task_4/RemoveFragment/";
	this->fName1 = "grimg1.bmp";
	this->fName2 = "grimg2.bmp";
	this->fName3 = "rgbimg1.bmp";
	this->fName4 = "rgbimg2.bmp";

	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes3 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName3, cv::IMREAD_COLOR);
	cv::Mat myRes4 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName4, cv::IMREAD_COLOR);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fName3, myRes3);
	cv::imshow(this->fName4, myRes4);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fName3);
	cv::destroyWindow(this->fName4);
}

void cv::ShowResult::ShowResultT_4_8()
{
	this->CurrentTaskPath = "Task_4/CopyFragment/";
	this->fName1 = "grimg1.bmp";
	this->fName2 = "grimg2.bmp";
	this->fName3 = "Cropped_grimg1.bmp";
	this->fName4 = "Cropped_grimg2.bmp";

	this->fNameN1 = "rgbimg1.bmp";
	this->fNameN2 = "rgbimg2.bmp";
	this->fNameN3 = "Cropped_rgbimg1.bmp";
	this->fNameN4 = "Cropped_rgbimg2.bmp";


	cv::Mat myRes1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName1, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName2, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes3 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName3, cv::IMREAD_GRAYSCALE);
	cv::Mat myRes4 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fName4, cv::IMREAD_GRAYSCALE);
	cv::Mat myResN1 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN1, cv::IMREAD_COLOR);
	cv::Mat myResN2 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN2, cv::IMREAD_COLOR);
	cv::Mat myResN3 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN3, cv::IMREAD_COLOR);
	cv::Mat myResN4 = cv::imread(this->ResultPath + this->CurrentTaskPath + this->fNameN4, cv::IMREAD_COLOR);

	cv::imshow(this->fGreyName1, this->fGrImg1);
	cv::imshow(this->fGreyName2, this->fGrImg2);
	cv::imshow(this->fRGBName1, this->fRGBImg1);
	cv::imshow(this->fRGBName2, this->fRGBImg2);
	cv::imshow(this->fName1, myRes1);
	cv::imshow(this->fName2, myRes2);
	cv::imshow(this->fName3, myRes3);
	cv::imshow(this->fName4, myRes4);
	cv::imshow(this->fNameN1, myResN1);
	cv::imshow(this->fNameN2, myResN2);
	cv::imshow(this->fNameN3, myResN3);
	cv::imshow(this->fNameN4, myResN4);
	cv::waitKey('27');
	cv::destroyWindow(this->fGreyName1);
	cv::destroyWindow(this->fGreyName2);
	cv::destroyWindow(this->fRGBName1);
	cv::destroyWindow(this->fRGBName2);
	cv::destroyWindow(this->fName1);
	cv::destroyWindow(this->fName2);
	cv::destroyWindow(this->fName3);
	cv::destroyWindow(this->fName4);
	cv::destroyWindow(this->fNameN1);
	cv::destroyWindow(this->fNameN2);
	cv::destroyWindow(this->fNameN3);
	cv::destroyWindow(this->fNameN4);
}
